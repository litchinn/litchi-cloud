package com.litchi.cloud.common.minio.service.impl;

import com.litchi.cloud.common.minio.service.IMinioUploadService;
import com.litchi.cloud.common.minio.utils.MinioUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MinioUploadServiceImpl implements IMinioUploadService {

    /**
     * @Author litchi
     * @Description 上传文件
     * @Date 16:40 2020/9/3
     * @param file 文件
     * @param bizPath 目录
     * @return java.lang.String 存储路径
     **/
    @Override
    public String upload(MultipartFile file, String bizPath) {
        return MinioUtil.upload(file, bizPath);
    }
}
