package com.litchi.cloud.common.minio.service;

import org.springframework.web.multipart.MultipartFile;

public interface IMinioUploadService {

    /**
     * @Author litchi
     * @Description 上传文件
     * @Date 16:32 2020/9/3
     * @param file 文件
     * @param bizPath 目录
     * @return java.lang.String 存储路径
     **/
    String upload(MultipartFile file, String bizPath);
}
