package com.litchi.cloud.system.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.result.Result;
import com.litchi.cloud.common.service.IUserService;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.common.vo.UserVO;
import com.litchi.cloud.system.entity.Organ;
import com.litchi.cloud.system.entity.UserRole;
import com.litchi.cloud.system.mapper.UserRoleMapper;
import com.litchi.cloud.system.service.IOrganService;
import com.litchi.cloud.system.service.IRoleService;
import com.litchi.cloud.system.service.ISysPermissionService;

import lombok.extern.slf4j.Slf4j;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 用户
 * @Author: jeecg-boot
 * @Date:   2020-04-29
 * @Version: V1.0
 */
@Slf4j
@Api(tags="用户")
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private IUserService userService;
	@Autowired
	private IOrganService organService;
	@Autowired
	private ISysPermissionService permissionService;
	@Autowired
	private IRoleService roleService;
	@Autowired
	private UserRoleMapper userRoleMapper;
	
	/**
	  * 分页列表查询
	 * @param user
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@ApiOperation(value="用户-分页列表查询", notes="用户-分页列表查询")
	@GetMapping(value = "/list")
	public PageResult<User> queryPageList(User user,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									  @RequestParam(name="beginTime", required = false) String startTime,
									  @RequestParam(name="endTime", required = false) String endTime,
									  HttpServletRequest req) {
		List<Integer> organIds = null;
		if(!StringUtils.isEmpty(user.getOrganId())) {
			QueryWrapper<Organ> queryWrapper2 = new QueryWrapper<Organ>();
			queryWrapper2.lambda().apply("FIND_IN_SET (" + user.getOrganId() + ", ancestors)");
			organIds = organService.list(queryWrapper2).stream().map(Organ::getId).collect(Collectors.toList());
		}
		QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
		queryWrapper.lambda()
			.eq(!StringUtils.isEmpty(user.getOrganId()), User::getOrganId, user.getOrganId())
			.like(!StringUtils.isEmpty(user.getUserName()), User::getUserName, user.getUserName())
			.like(!StringUtils.isEmpty(user.getPhonenumber()), User::getPhonenumber, user.getPhonenumber())
			.eq(!StringUtils.isEmpty(user.getStatus()), User::getStatus, user.getStatus())
			.between(!StringUtils.isEmpty(startTime), User::getCreateTime, startTime, endTime)
			.or().in(!CollectionUtils.isEmpty(organIds), User::getOrganId, organIds);
		Page<User> page = new Page<User>(pageNo, pageSize);
		IPage<User> pageList = userService.page(page, queryWrapper);
		return new PageResult<User>(pageList.getRecords(), pageList.getTotal());
	}
	
	/**
	  *   添加
	 * @param user
	 * @return
	 */
	@ApiOperation(value="用户-添加", notes="用户-添加")
	@PostMapping()
	public AjaxResult add(@RequestBody UserVO user) {
		return userService.createUser(user);
	}
	
	/**
	  *  编辑
	 * @param user
	 * @return
	 */
	@ApiOperation(value="用户-编辑", notes="用户-编辑")
	@PutMapping()
	public AjaxResult edit(@RequestBody UserVO user) {
		return userService.update(user);
	}
	
	/**
	  *   通过id删除
	 * @param id
	 * @return
	 */
	@ApiOperation(value="用户-通过id删除", notes="用户-通过id删除")
	@DeleteMapping("/{id}")
	public Result<?> delete(@PathVariable() Integer id) {
		try {
			QueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>();
	        queryWrapper.lambda().eq(UserRole::getUserId, id);
	        userRoleMapper.delete(queryWrapper);
			userService.removeById(id);
		} catch (Exception e) {
			log.error("删除失败",e.getMessage());
			return Result.error("删除失败!");
		}
		return Result.ok("删除成功!");
	}
	
	/**
	  *  批量删除
	 * @param ids
	 * @return
	 */
	@PreAuthorize("@pms.hasPermission('system:user:remove')")
	@ApiOperation(value="用户-批量删除", notes="用户-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<User> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		Result<User> result = new Result<User>();
		if(ids==null || "".equals(ids.trim())) {
			result.error500("参数不识别！");
		}else {
			this.userService.removeByIds(Arrays.asList(ids.split(",")));
			result.success("删除成功!");
		}
		return result;
	}
	
	/**
	  * 通过id查询
	 * @param id
	 * @return
	 */
	@ApiOperation(value="用户-通过id查询", notes="用户-通过id查询")
	@GetMapping(value = { "/", "/{id}" })
	public AjaxResult queryById(@PathVariable(value = "id", required = false) Integer id) {
		 AjaxResult ajax = AjaxResult.success();
	        ajax.put("roles", roleService.list());
	        if (!StringUtils.isEmpty(id)) {
	            ajax.put(AjaxResult.DATA_TAG, userService.getById(id));
	            QueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>();
	            queryWrapper.lambda().eq(UserRole::getUserId, id);
	            ajax.put("roleIds", userRoleMapper.selectList(queryWrapper).stream().map(UserRole::getRoleId).collect(Collectors.toList()));
	        }
	    return ajax;
	}
	
	//@PreAuthorize("@pms.hasPermission('system:user:query')")
	@GetMapping("/info")
	public AjaxResult getUserByLoginName() {
		String loginName = WebContextUtil.getCurrUsername();
        LoginUser uu = userService.getUserByUsername(loginName);
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(uu);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(uu);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", uu);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

	@GetMapping("/userinfo")
	public Principal currentUser(Principal user) {
		return user;
	}
	
	@GetMapping("/export")
    public void export(User user,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  @RequestParam(name="beginTime", required = false) String startTime,
			  @RequestParam(name="endTime", required = false) String endTime,
			  HttpServletRequest request, HttpServletResponse response) throws IOException {
    	String userName = WebContextUtil.getUsername();
    	QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
    	queryWrapper.lambda()
			.between(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime), User::getCreateTime, startTime, endTime)
			.like(!StringUtils.isEmpty(user.getUserName()), User::getUserName, user.getUserName())
			.like(!StringUtils.isEmpty(user.getPhonenumber()), User::getPhonenumber, user.getPhonenumber())
			.eq(!StringUtils.isEmpty(user.getStatus()), User::getStatus, user.getStatus());
        //Step.2 AutoPoi 导出Excel
        List<User> pageList = userService.list(queryWrapper);
        //导出文件名称
        HSSFWorkbook workbook = (HSSFWorkbook) ExcelExportUtil.exportExcel(new ExportParams("用户列表数据", "导出人:" + userName, "用户导出"), User.class, pageList);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment;filename="+URLEncoder.encode("用户列表.xls", "utf-8"));
        OutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
