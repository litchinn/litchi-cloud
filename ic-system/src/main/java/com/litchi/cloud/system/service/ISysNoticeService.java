package com.litchi.cloud.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.SysNotice;

/**
 * @Description: 通知公告
 * @Author: jeecg-boot
 * @Date:   2020-05-27
 * @Version: V1.0
 */
public interface ISysNoticeService extends IService<SysNotice> {

}
