package com.litchi.cloud.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.SysDictData;

/**
 * @Description: 字典数据
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ISysDictDataService extends IService<SysDictData> {

}
