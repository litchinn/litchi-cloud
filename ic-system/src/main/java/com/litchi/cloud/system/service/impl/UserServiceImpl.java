package com.litchi.cloud.system.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.constants.Oauth2Constant;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.exception.CustomException;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.service.IUserService;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.common.vo.UserInfo;
import com.litchi.cloud.common.vo.UserVO;
import com.litchi.cloud.system.entity.Resource;
import com.litchi.cloud.system.entity.Role;
import com.litchi.cloud.system.entity.UserRole;
import com.litchi.cloud.system.mapper.ResourceMapper;
import com.litchi.cloud.system.mapper.RoleMapper;
import com.litchi.cloud.system.mapper.UserMapper;
import com.litchi.cloud.system.mapper.UserRoleMapper;

/**
 * @Description: 用户
 * @Author: jeecg-boot
 * @Date:   2020-04-29
 * @Version: V1.0
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
	
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private ResourceMapper resourceMapper;

	private static final PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	
	@Override
	public AjaxResult createUser(UserVO user) {
		if (UserConstants.NOT_UNIQUE.equals(checkUserNameUnique(user.getUserName())))
        {
            return AjaxResult.error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(checkPhoneUnique(user)))
        {
            return AjaxResult.error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(checkEmailUnique(user)))
        {
            return AjaxResult.error("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
		user.setCreateTime(new Date());
		user.setCreateBy(WebContextUtil.getUsername());
		user.setPassword(encoder.encode(user.getPassword()));
		super.save(user);
		if(!CollectionUtils.isEmpty(user.getRoleIds())) {
			insertUserRole(user.getRoleIds(), user.getId());
		}
		return AjaxResult.success();
	}
	
	private void insertUserRole(List<Integer> list, Integer userId) {
		for(Integer roleId : list) {
			UserRole ur = new UserRole();
			ur.setUserId(userId);
			ur.setRoleId(roleId);
			userRoleMapper.insert(ur);
		}
	}
	
    public String checkUserNameUnique(String userName) {
    	QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
    	queryWrapper.lambda().eq(User::getUserName, userName);
        int count = userMapper.selectCount(queryWrapper);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验电话号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    public String checkPhoneUnique(User user) {
        Integer userId = StringUtils.isEmpty(user.getId()) ? -1 : user.getId();
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
    	queryWrapper.lambda().eq(User::getPhonenumber, user.getPhonenumber());
        User info = userMapper.selectOne(queryWrapper);
        if (!StringUtils.isEmpty(info) && info.getId() != userId) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    public String checkEmailUnique(User user) {
        Integer userId = StringUtils.isEmpty(user.getId()) ? -1 : user.getId();
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
    	queryWrapper.lambda().eq(User::getPhonenumber, user.getPhonenumber());
        User info = userMapper.selectOne(queryWrapper);
        if (!StringUtils.isEmpty(info) && info.getId() != userId) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

	@Override
	public AjaxResult update(UserVO user) {
		checkUserAllowed(user);
        if (UserConstants.NOT_UNIQUE.equals(checkPhoneUnique(user)))
        {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(checkEmailUnique(user)))
        {
            return AjaxResult.error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        QueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>();
        queryWrapper.lambda().eq(UserRole::getUserId, user.getId());
        userRoleMapper.delete(queryWrapper);
        if(!CollectionUtils.isEmpty(user.getRoleIds())) {
			insertUserRole(user.getRoleIds(), user.getId());
		}
        user.setUpdateBy(WebContextUtil.getUsername());
        baseMapper.updateById(user);
        return AjaxResult.success();
	}

	private void checkUserAllowed(User user) {
		if (!StringUtils.isEmpty(user.getId()) && user.isAdmin()) {
            throw new CustomException("不允许操作超级管理员用户");
        }
	}

	@Override
	public LoginUser getUserByUsername(String loginName) {
		// TODO Auto-generated method stub
		return getUserDetails(getUserInfoByUsername(loginName));
	}
	
	
	/**
	 * 构建userdetails
	 *
	 * @param result 用户信息
	 * @return
	 */
	private LoginUser getUserDetails(UserInfo info) {
		if (info == null) {
			throw new UsernameNotFoundException("用户不存在");
		}
		Set<String> dbAuthsSet = new HashSet<>();
		if (ArrayUtils.isNotEmpty(info.getRoles())) {
			// 获取角色
			Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(Oauth2Constant.ROLE + role));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

		}
		Collection<? extends GrantedAuthority> authorities
			= AuthorityUtils.createAuthorityList(dbAuthsSet.toArray(new String[0]));
		User user = info.getUser();

		// 构造security用户
		return new LoginUser(user.getId(), user.getOrganId(), user.getUserName(), user.getPassword(), user.getNickName(), "", user.getAvatar(),
				UserConstants.NORMAL.equals(user.getDelFlag()), true, true, true, authorities);
	}
	 /**
     * 查询用户所属角色组
     * 
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName)
    {
        List<Role> list = roleMapper.selectRolesByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (Role role : list)
        {
            idsStr.append(role.getName()).append(",");
        }
        if (!StringUtils.isEmpty(idsStr.toString()))
        {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

	@Override
	public UserInfo getUserInfoByUsername(String username) {
		QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
		queryWrapper.lambda().eq(User::getUserName, username);
		User user = userMapper.selectOne(queryWrapper);
		List<Integer> roleIds = userRoleMapper.getUserRoles(user.getId());
		QueryWrapper<Role> queryWrapper1 = new QueryWrapper<Role>();
		queryWrapper1.lambda().in(Role::getId, roleIds);
		List<Role> roles = roleMapper.selectList(queryWrapper1);
		UserInfo userInfo = new UserInfo();
		List<String> roleKeys = roles.stream().map(Role::getRoleKey).collect(Collectors.toList());
		userInfo.setRoles(roleKeys.toArray(new String[roleKeys.size()]));
		List<String> perms = resourceMapper.selectPermsByRoles(roleIds);
		if(roleIds.contains(1)) {
			QueryWrapper<Resource> rq = new QueryWrapper<Resource>();
			rq.lambda().ne(Resource::getPerms, "");
			List<String> adminPerms = resourceMapper.selectList(rq).stream().map(Resource::getPerms).collect(Collectors.toList());
			perms.addAll(adminPerms);
			Set<String> set = new HashSet<>();
			set.addAll(perms);
			perms = set.stream().collect(Collectors.toList());
		}
		userInfo.setPermissions(perms.toArray(new String[perms.size()]));
		com.litchi.cloud.common.vo.User u = new com.litchi.cloud.common.vo.User();
		BeanUtils.copyProperties(user, u);
		userInfo.setUser(u);
		return userInfo;
	}

}
