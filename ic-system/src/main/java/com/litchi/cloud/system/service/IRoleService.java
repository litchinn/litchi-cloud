package com.litchi.cloud.system.service;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.Role;
import com.litchi.cloud.system.vo.RoleVO;

/**
 * @Description: 角色
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IRoleService extends IService<Role> {

	Set<String> selectRolePermissionByUserId(Integer id);

	Role selectRoleById(Integer id);

	String checkRoleNameUnique(Role role);

	String checkRoleKeyUnique(Role role);

	void deleteByIds(List<Integer> roleIds);

	void checkRoleAllowed(Role role);

	int updateRole(RoleVO roleVO);

	int insertRoleResource(RoleVO role);

	int authDataScope(RoleVO role);

	List<Role> selectAll();

}
