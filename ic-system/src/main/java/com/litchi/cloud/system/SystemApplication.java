package com.litchi.cloud.system;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

@SpringBootApplication(scanBasePackages = { "com.litchi.cloud.common", "com.litchi.cloud.system" })
@EnableDiscoveryClient
@MapperScan("com.litchi.cloud.system.mapper")
@DubboComponentScan("com.litchi.cloud.system.service")
@EnableAutoConfiguration
@EnableOAuth2Client
public class SystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemApplication.class, args);
	}

}
