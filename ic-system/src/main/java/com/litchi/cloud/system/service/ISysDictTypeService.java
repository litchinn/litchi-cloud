package com.litchi.cloud.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.SysDictType;

/**
 * @Description: 字典类型
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ISysDictTypeService extends IService<SysDictType> {

	String checkDictTypeUnique(SysDictType dict);

}
