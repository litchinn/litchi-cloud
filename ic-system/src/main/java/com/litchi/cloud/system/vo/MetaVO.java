package com.litchi.cloud.system.vo;
/** 
 * 路由显示信息
 * @author: wjhf@litchi 
 * @date: 2020年5月13日 上午11:45:12
 * @vesion: 1.0
 */ 
public class MetaVO
{
    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    private String title;

    /**
     * 设置该路由的图标，对应路径src/icons/svg
     */
    private String icon;

    public MetaVO()
    {
    }

    public MetaVO(String title, String icon)
    {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }
}
