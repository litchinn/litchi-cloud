package com.litchi.cloud.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.system.entity.Resource;
import com.litchi.cloud.system.entity.RoleResource;
import com.litchi.cloud.system.mapper.ResourceMapper;
import com.litchi.cloud.system.mapper.RoleResourceMapper;
import com.litchi.cloud.system.service.IResourceService;
import com.litchi.cloud.system.vo.MetaVO;
import com.litchi.cloud.system.vo.ResourceVO;
import com.litchi.cloud.system.vo.RouterVO;
import com.litchi.cloud.system.vo.TreeSelect;

/**
 * @Description: 资源菜单
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {
	
	@Autowired
	private ResourceMapper resourceMapper;
	@Autowired
	private RoleResourceMapper roleResourceMapper;

	@SuppressWarnings("unchecked")
	@Override
    public List<ResourceVO> selectMenuTreeByUserId(Integer userId)
    {
        List<Resource> menus = null;
        if (WebContextUtil.isAdmin(userId)){
        	QueryWrapper<Resource> queryWrapper = new QueryWrapper<Resource>();
        	queryWrapper.lambda()
        		.in(Resource::getMenuType, Arrays.asList("M","C")).eq(Resource::getStatus, 0)
        		.orderByAsc(Resource::getParentId, Resource::getOrderNum);
            menus = this.baseMapper.selectList(queryWrapper);
        }
        else
        {
            menus = this.baseMapper.selectMenuTreeByUserId(userId);
        }
        List<ResourceVO> menusVO = new ArrayList<ResourceVO>();
        for(Resource r : menus) {
        	ResourceVO v = new ResourceVO();
        	BeanUtils.copyProperties(r, v);
        	menusVO.add(v);
        }
        return getChildPerms(menusVO, 0);
    }
	
	/**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list 分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<ResourceVO> getChildPerms(List<ResourceVO> list, int parentId)
    {
        List<ResourceVO> returnList = new ArrayList<ResourceVO>();
        for (Iterator<ResourceVO> iterator = list.iterator(); iterator.hasNext();)
        {
        	ResourceVO t = (ResourceVO) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId)
            {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }
    
    /**
     * 递归列表
     * 
     * @param list
     * @param t
     */
    private void recursionFn(List<ResourceVO> list, ResourceVO t)
    {
        // 得到子节点列表
        List<ResourceVO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (ResourceVO tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                // 判断是否有子节点
                Iterator<ResourceVO> it = childList.iterator();
                while (it.hasNext())
                {
                	ResourceVO n = (ResourceVO) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }
    
    /**
     * 得到子节点列表
     */
    private List<ResourceVO> getChildList(List<ResourceVO> list, ResourceVO t)
    {
        List<ResourceVO> tlist = new ArrayList<ResourceVO>();
        Iterator<ResourceVO> it = list.iterator();
        while (it.hasNext())
        {
        	ResourceVO n = (ResourceVO) it.next();
            if (n.getParentId().longValue() == t.getId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<ResourceVO> list, ResourceVO t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }

	/**
     * 构建前端路由所需要的菜单
     * 
     * @param menus 菜单列表
     * @return 路由列表
     */
    @Override
    public List<RouterVO> buildMenus(List<ResourceVO> menus)
    {
        List<RouterVO> routers = new LinkedList<RouterVO>();
        for (ResourceVO menu : menus)
        {
        	RouterVO router = new RouterVO();
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(StringUtils.capitalize(menu.getPath()));
            router.setPath(getRouterPath(menu));
            router.setComponent(StringUtils.isEmpty(menu.getComponent()) ? "Layout" : menu.getComponent());
            router.setMeta(new MetaVO(menu.getName(), menu.getIcon()));
            List<ResourceVO> cMenus = menu.getChildren();
            if (!cMenus.isEmpty() && cMenus.size() > 0 && "M".equals(menu.getMenuType()))
            {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            }
            routers.add(router);
        }
        return routers;
    }
    
    /**
     * 获取路由地址
     * 
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(Resource menu)
    {
        String routerPath = menu.getPath();
        // 非外链并且是一级目录
        if (0 == menu.getParentId() && "1".equals(menu.getIsFrame().toString()))
        {
            routerPath = "/" + menu.getPath();
        }
        return routerPath;
    }

	@Override
	public Set<String> selectResourcePermsByUserId(Integer id) {
		List<String> perms = resourceMapper.selectMenuPermsByUserId(id);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms)
        {
            if (!StringUtils.isEmpty(perm))
            {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
	}

	@Override
	public List<TreeSelect> buildResourceTreeSelect(List<ResourceVO> menus) {
		List<ResourceVO> menuTrees = buildMenuTree(menus);
        return menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
	}
	
	public List<ResourceVO> buildMenuTree(List<ResourceVO> menus)
    {
        List<ResourceVO> returnList = new ArrayList<ResourceVO>();
        for (Iterator<ResourceVO> iterator = menus.iterator(); iterator.hasNext();)
        {
        	ResourceVO t = (ResourceVO) iterator.next();
            // 根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == 0)
            {
                recursionFn(menus, t);
                returnList.add(t);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = menus;
        }
        return returnList;
    }
	
	@Override
    public List<ResourceVO> selectResourceList(Integer userId)
    {
        return selectResourceList(new Resource(), userId);
    }

    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<ResourceVO> selectResourceList(Resource menu, Integer userId) {
        List<Resource> menuList = null;
        // 管理员显示所有菜单信息
        if (User.isAdmin(userId)) {
        	QueryWrapper<Resource> queryWrapper = new QueryWrapper<Resource>();
        	queryWrapper.lambda().like(!StringUtils.isEmpty(menu.getName()), Resource::getName, menu.getName())
        	.like(!StringUtils.isEmpty(menu.getVisible()), Resource::getVisible, menu.getVisible())
        	.like(!StringUtils.isEmpty(menu.getStatus()), Resource::getStatus, menu.getStatus())
        	.orderByAsc(Resource::getParentId, Resource::getOrderNum);
            menuList = resourceMapper.selectList(queryWrapper);
        }
        else {
            menuList = resourceMapper.selectResourceListByUserId(menu, userId);
        }
        List<ResourceVO> result = new ArrayList<ResourceVO>();
        for(Resource r : menuList) {
        	ResourceVO v = new ResourceVO();
        	BeanUtils.copyProperties(r, v);
        	result.add(v);
        }
        return result;
    }

	@Override
	public List<Integer> selectResourceListByRoleId(Integer roleId) {
		return resourceMapper.selectResourceListByRoleId(roleId);
	}

	 /**
     * 校验菜单名称是否唯一
     * 
     * @param resource 菜单信息
     * @return 结果
     */
    @Override
    public String checkResourceNameUnique(ResourceVO resource)
    {
        Integer menuId = StringUtils.isEmpty(resource.getId()) ? -1 : resource.getId();
        QueryWrapper<Resource> queryWrapper = new QueryWrapper<Resource>();
        queryWrapper.lambda().eq(Resource::getName, resource.getName()).eq(Resource::getParentId, resource.getParentId());
        Resource info = resourceMapper.selectOne(queryWrapper);
        if (!StringUtils.isEmpty(info) && info.getId().intValue() != menuId.intValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

	@Override
	public boolean hasChildByResourceId(Integer id) {
		QueryWrapper<Resource> queryWrapper = new QueryWrapper<Resource>();
		queryWrapper.lambda().eq(Resource::getParentId, id);
		int n = resourceMapper.selectCount(queryWrapper);
		return n > 0;
	}

	@Override
	public boolean checkResourceExistRole(Integer id) {
		QueryWrapper<RoleResource> queryWrapper = new QueryWrapper<RoleResource>();
		queryWrapper.lambda().eq(RoleResource::getResourceId, id);
		int n = roleResourceMapper.selectCount(queryWrapper);
		return n > 0;
	}
}
