package com.litchi.cloud.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.system.entity.SysNotice;
import com.litchi.cloud.system.mapper.SysNoticeMapper;
import com.litchi.cloud.system.service.ISysNoticeService;

/**
 * @Description: 通知公告
 * @Author: jeecg-boot
 * @Date:   2020-05-27
 * @Version: V1.0
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {

}
