package com.litchi.cloud.system.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.system.entity.Resource;
import com.litchi.cloud.system.service.IResourceService;
import com.litchi.cloud.system.vo.ResourceVO;

import io.swagger.annotations.Api;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wjhf
 * @since 2019-08-08
 */
@RestController
@RequestMapping("/resource")
@Api(value = "ResourceController|资源菜单相关的控制器", tags = { "资源菜单相关接口" })
public class ResourceController {

	@Autowired
	private IResourceService resourceService;

	/**
	 * 获取路由信息
	 * 
	 * @return 路由信息
	 */
	@GetMapping("getRouters")
	public AjaxResult getRouters() {
		SecurityContextHolder.getContext();
		Integer userId = WebContextUtil.getCurrUid();
		List<ResourceVO> menus = resourceService.selectMenuTreeByUserId(userId);
		return AjaxResult.success(resourceService.buildMenus(menus));
	}

	/**
	 * 获取菜单下拉树列表
	 */
	@GetMapping("/treeselect")
	public AjaxResult treeselect(ResourceVO menu) {
		Integer userId = WebContextUtil.getCurrUid();
		List<ResourceVO> menus = resourceService.selectResourceList(userId);
		return AjaxResult.success(resourceService.buildResourceTreeSelect(menus));
	}

	/**
	 * 加载对应角色菜单列表树
	 */
	@GetMapping(value = "/roleResourceTreeselect/{roleId}")
	public AjaxResult roleMenuTreeselect(@PathVariable("roleId") Integer roleId) {
		Integer userId = WebContextUtil.getCurrUid();
		List<ResourceVO> menus = resourceService.selectResourceList(userId);
		AjaxResult ajax = AjaxResult.success();
		ajax.put("checkedKeys", resourceService.selectResourceListByRoleId(roleId));
		ajax.put("menus", resourceService.buildResourceTreeSelect(menus));
		return ajax;
	}

	/**
	 * 获取菜单列表
	 */
	@GetMapping("/list")
	public AjaxResult list(Resource menu) {
		Integer userId = WebContextUtil.getCurrUid();
		List<ResourceVO> menus = resourceService.selectResourceList(menu, userId);
		return AjaxResult.success(menus);
	}

	/**
	 * 根据菜单编号获取详细信息
	 */
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable Integer id) {
		return AjaxResult.success(resourceService.getById(id));
	}

	/**
	 * 新增菜单
	 */
	@PostMapping
	public AjaxResult add(@Validated @RequestBody ResourceVO menu) {
		if (UserConstants.NOT_UNIQUE.equals(resourceService.checkResourceNameUnique(menu))) {
			return AjaxResult.error("新增菜单'" + menu.getName() + "'失败，菜单名称已存在");
		}
		menu.setCreateBy(WebContextUtil.getUsername());
		menu.setCreateTime(new Date());
		resourceService.save(menu);
		return AjaxResult.success();
	}

	/**
	 * 修改菜单
	 */
	@PutMapping
	public AjaxResult edit(@Validated @RequestBody ResourceVO menu) {
		if (UserConstants.NOT_UNIQUE.equals(resourceService.checkResourceNameUnique(menu))) {
			return AjaxResult.error("修改菜单'" + menu.getName() + "'失败，菜单名称已存在");
		}
		menu.setUpdateBy(WebContextUtil.getUsername());
		menu.setUpdateTime(new Date());
		resourceService.updateById(menu);
		return AjaxResult.success();
	}

	/**
	 * 删除菜单
	 */
	@DeleteMapping("/{id}")
	public AjaxResult remove(@PathVariable Integer id) {
		if (resourceService.hasChildByResourceId(id)) {
			return AjaxResult.error("存在子菜单,不允许删除");
		}
		if (resourceService.checkResourceExistRole(id)) {
			return AjaxResult.error("菜单已分配,不允许删除");
		}
		resourceService.removeById(id);
		return AjaxResult.success();
	}
}
