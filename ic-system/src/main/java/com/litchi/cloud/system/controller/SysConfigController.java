package com.litchi.cloud.system.controller;

import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.utils.query.QueryGenerator;
import com.litchi.cloud.system.entity.SysConfig;
import com.litchi.cloud.system.service.ISysConfigService;

/**
 * 参数配置 信息操作处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/config")
public class SysConfigController {
	@Autowired
	private ISysConfigService configService;

	/**
	 * 获取参数配置列表
	 */
	@GetMapping("/list")
	public PageResult<SysConfig> list(SysConfig config,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "startTime", required = false) String startTime,
			@RequestParam(name = "endTime", required = false) String endTime, HttpServletRequest req) {
		QueryWrapper<SysConfig> queryWrapper = QueryGenerator.initQueryWrapper(config, req.getParameterMap());
		Page<SysConfig> page = new Page<SysConfig>(pageNo, pageSize);
		IPage<SysConfig> list = configService.page(page, queryWrapper);
		return new PageResult<SysConfig>(list.getRecords(), list.getTotal());
	}

	/**
	 * 根据参数编号获取详细信息
	 */
	@GetMapping(value = "/{configId}")
	public AjaxResult getInfo(@PathVariable Integer configId) {
		return AjaxResult.success(configService.getById(configId));
	}

	/**
	 * 根据参数键名查询参数值
	 */
	@GetMapping(value = "/configKey/{configKey}")
	public AjaxResult getConfigKey(@PathVariable String configKey) {
		QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<SysConfig>();
		queryWrapper.lambda().eq(SysConfig::getConfigKey, configKey);
		return AjaxResult.success(configService.getOne(queryWrapper));
	}

	/**
	 * 新增参数配置
	 */
	@PostMapping
	public AjaxResult add(@Validated @RequestBody SysConfig config) {
		if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
			return AjaxResult.error("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
		}
		config.setCreateBy(WebContextUtil.getUsername());
		config.setCreateTime(new Date());
		configService.save(config);
		return AjaxResult.success();
	}

	/**
	 * 修改参数配置
	 */
	@PutMapping
	public AjaxResult edit(@Validated @RequestBody SysConfig config) {
		if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
			return AjaxResult.error("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
		}
		config.setUpdateBy(WebContextUtil.getUsername());
		config.setUpdateTime(new Date());
		configService.updateById(config);
		return AjaxResult.success();
	}

	/**
	 * 删除参数配置
	 */
	@DeleteMapping("/{configIds}")
	public AjaxResult remove(@PathVariable Integer[] configIds) {
		configService.removeByIds(Arrays.asList(configIds));
		return AjaxResult.success();
	}
}
