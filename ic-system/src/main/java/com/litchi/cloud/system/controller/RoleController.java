package com.litchi.cloud.system.controller;

import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.system.entity.Role;
import com.litchi.cloud.system.service.IRoleService;
import com.litchi.cloud.system.vo.RoleVO;

import io.swagger.annotations.ApiOperation;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月18日 上午10:37:16
 * @vesion: 1.0
 */
@RestController
@RequestMapping("role")
public class RoleController {

	@Autowired
	private IRoleService roleService;
	
    /**
	  * 分页列表查询
	 * @param role
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@ApiOperation(value="角色-分页列表查询", notes="角色-分页列表查询")
	@GetMapping(value = "/list")
	public PageResult<User> queryPageList(Role role,
									  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									  @RequestParam(name="beginTime", required = false) String startTime,
									  @RequestParam(name="endTime", required = false) String endTime,
									  HttpServletRequest req) {
		QueryWrapper<Role> queryWrapper = new QueryWrapper<Role>();
		queryWrapper.lambda()
			.like(!StringUtils.isEmpty(role.getName()), Role::getName, role.getName())
			.like(!StringUtils.isEmpty(role.getRoleKey()), Role::getRoleKey, role.getRoleKey())
			.eq(!StringUtils.isEmpty(role.getStatus()), Role::getStatus, role.getStatus())
			.between(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime), Role::getCreateTime, startTime, endTime);
		Page<Role> page = new Page<Role>(pageNo, pageSize);
		IPage<Role> pageList = roleService.page(page, queryWrapper);
		return new PageResult<User>(pageList.getRecords(), pageList.getTotal());
	}
	
	/**
     * 根据角色编号获取详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable Integer id) {
        return AjaxResult.success(roleService.selectRoleById(id));
    }
    
    /** 
     * 新增角色
     * @param role
     * @return 
     * 返回类型  AjaxResult 
     */
    @PostMapping
    public AjaxResult add(@Validated @RequestBody RoleVO role) {
        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return AjaxResult.error("新增角色'" + role.getName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return AjaxResult.error("新增角色'" + role.getName() + "'失败，角色权限已存在");
        }
        role.setCreateBy(WebContextUtil.getUsername());
        role.setCreateTime(new Date());
        roleService.save(role);
        roleService.insertRoleResource(role);
        return AjaxResult.success();

    }
    
    /**
     * 删除角色
     */
    @DeleteMapping("/{roleIds}")
    public AjaxResult remove(@PathVariable Integer[] roleIds) {
    	roleService.deleteByIds(Arrays.asList(roleIds));
        return AjaxResult.success();
    }
    
    /**
     * 修改保存角色
     */
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody RoleVO role) {
        roleService.checkRoleAllowed(role);
        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
            return AjaxResult.error("修改角色'" + role.getName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
            return AjaxResult.error("修改角色'" + role.getName() + "'失败，角色权限已存在");
        }
        role.setUpdateBy(WebContextUtil.getUsername());
        role.setUpdateTime(new Date());
        roleService.updateRole(role);
        return AjaxResult.success();
    }
    
    /**
     * 修改保存数据权限
     */
    @PutMapping("/dataScope")
    public AjaxResult dataScope(@RequestBody RoleVO role) {
        roleService.checkRoleAllowed(role);
        roleService.authDataScope(role);
        return AjaxResult.success();
    }

    /**
     * 状态修改
     */
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody Role role) {
        roleService.checkRoleAllowed(role);
        role.setUpdateBy(WebContextUtil.getUsername());
        roleService.updateById(role);
        return AjaxResult.success();
    }
    
    /**
     * 获取角色选择框列表
     */
    @GetMapping("/optionselect")
    public AjaxResult optionselect() {
        return AjaxResult.success(roleService.selectAll());
    }
}
