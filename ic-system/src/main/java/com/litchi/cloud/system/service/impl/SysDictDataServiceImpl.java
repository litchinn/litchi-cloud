package com.litchi.cloud.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.system.entity.SysDictData;
import com.litchi.cloud.system.mapper.SysDictDataMapper;
import com.litchi.cloud.system.service.ISysDictDataService;

/**
 * @Description: 字典数据
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements ISysDictDataService {

}
