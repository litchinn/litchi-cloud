package com.litchi.cloud.system.mapper;

import java.util.Map;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.vo.User;

/**
 * @Description: 用户
 * @Author: jeecg-boot
 * @Date:   2020-04-29
 * @Version: V1.0
 */
public interface UserMapper extends BaseMapper<User> {

	/** 
	 * 登录名获取用户
	 * @param username 用户名
	 * @return 
	 * 返回类型  LoginUser 
	 */
	LoginUser getUserByLoginName(String username);

	Map<String, Object> getUserByUsername(String username);

}
