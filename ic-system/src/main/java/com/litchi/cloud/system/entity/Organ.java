package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 机构
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Data
@TableName("organ")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="organ对象", description="机构")
public class Organ {
    
	/**部门id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "部门id")
	private java.lang.Integer id;
	/**父部门id*/
    @ApiModelProperty(value = "父部门id")
	private java.lang.Integer parentId;
	/**祖级列表*/
    @ApiModelProperty(value = "祖级列表")
	private java.lang.String ancestors;
	/**部门名称*/
    @ApiModelProperty(value = "部门名称")
	private java.lang.String organName;
	/**显示顺序*/
    @ApiModelProperty(value = "显示顺序")
	private java.lang.Integer orderNum;
	/**负责人*/
    @ApiModelProperty(value = "负责人")
	private java.lang.String leader;
	/**联系电话*/
    @ApiModelProperty(value = "联系电话")
	private java.lang.String phone;
	/**邮箱*/
    @ApiModelProperty(value = "邮箱")
	private java.lang.String email;
	/**部门状态（0正常 1停用）*/
    @ApiModelProperty(value = "部门状态（0正常 1停用）")
	private java.lang.String status;
	/**删除标志（0代表存在 2代表删除）*/
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
	private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
}
