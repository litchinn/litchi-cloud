package com.litchi.cloud.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.SysDictData;

/**
 * @Description: 字典数据
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface SysDictDataMapper extends BaseMapper<SysDictData> {

}
