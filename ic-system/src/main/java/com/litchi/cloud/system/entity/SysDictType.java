package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 字典类型
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("sys_dict_type")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="sys_dict_type对象", description="字典类型")
public class SysDictType {
    
	/**字典主键*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "字典主键")
	private java.lang.Integer dictId;
	/**字典名称*/
    @ApiModelProperty(value = "字典名称")
	private java.lang.String dictName;
	/**字典类型*/
    @ApiModelProperty(value = "字典类型")
	private java.lang.String dictType;
	/**状态（0正常 1停用）*/
    @ApiModelProperty(value = "状态（0正常 1停用）")
	private java.lang.String status;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
	private java.lang.String remark;
}
