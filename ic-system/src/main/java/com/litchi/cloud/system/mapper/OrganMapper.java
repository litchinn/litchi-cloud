package com.litchi.cloud.system.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.Organ;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wjhf
 * @since 2019-08-10
 */
public interface OrganMapper extends BaseMapper<Organ> {

	List<Integer> selectOrganListByRoleId(Integer roleId);

	int selectNormalChildrenOrganById(Integer id);

	int checkOrganExistUser(Integer id);

	Organ checkOrganNameUnique(@Param("organName") String organName, @Param("parentId") Integer parentId);

	void updateOrganChildren(List<Organ> children);

	List<Organ> selectChildrenOrganById(Integer id);

	void updateOrganStatus(Organ dept);

}
