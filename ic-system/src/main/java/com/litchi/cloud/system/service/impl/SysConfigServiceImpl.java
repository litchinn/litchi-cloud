package com.litchi.cloud.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.system.entity.SysConfig;
import com.litchi.cloud.system.mapper.SysConfigMapper;
import com.litchi.cloud.system.service.ISysConfigService;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

	@Override
	public String checkConfigKeyUnique(SysConfig config) {
		Integer configId = StringUtils.isEmpty(config.getConfigId()) ? -1 : config.getConfigId();
		QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<SysConfig>();
		queryWrapper.lambda().eq(SysConfig::getConfigKey, config.getConfigKey());
        SysConfig info = this.baseMapper.selectOne(queryWrapper);
        if (!StringUtils.isEmpty(info) && info.getConfigId().longValue() != configId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
	}

}
