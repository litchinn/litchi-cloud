package com.litchi.cloud.system.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.common.result.Result;
import com.litchi.cloud.system.entity.Organ;
import com.litchi.cloud.system.vo.OrganVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wjhf
 * @since 2019-08-10
 */
public interface IOrganService extends IService<Organ> {

	/** 
	 * 通过id删除机构
	 * @param id
	 * @return 
	 * 返回类型  Result<String> 
	 */
	Result<String> delete(Integer id);

	/** 
	 * 根据id获取机构
	 * @param id
	 * @return 
	 * 返回类型  Result<OrganVO> 
	 */
	Result<OrganVO> getOrganById(Integer id);

	Object buildOrganTreeSelect(List<OrganVO> organ);

	Object selectOrganListByRoleId(Integer roleId);

	boolean hasChildById(Integer id);

	boolean checkOrganExistUser(Integer id);

	int selectNormalChildrenOrganById(Integer id);

	Object checkOrganNameUnique(Organ organ);

	int insertOrgan(Organ dept);

	int updateOrgan(Organ dept);

	int create(Organ organ);

}
