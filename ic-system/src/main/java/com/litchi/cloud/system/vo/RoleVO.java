package com.litchi.cloud.system.vo;

import com.litchi.cloud.system.entity.Role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * 角色类VO
 * @author: wjhf-litchi
 * @date: 2019年8月11日 下午3:28:33
 * @vesion: 0.0.1
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel(value="RoleVO",description="角色对象VO")
public class RoleVO extends Role{

	/** 菜单组 */
	@ApiModelProperty(value="菜单组")
    private Integer[] resourceIds;

    /** 机构组（数据权限） */
	@ApiModelProperty(value="机构组")
    private Integer[] organIds;
}
