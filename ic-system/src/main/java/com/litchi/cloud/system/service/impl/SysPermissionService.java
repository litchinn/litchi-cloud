package com.litchi.cloud.system.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.system.service.IResourceService;
import com.litchi.cloud.system.service.IRoleService;
import com.litchi.cloud.system.service.ISysPermissionService;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月14日 下午4:53:19
 * @vesion: 1.0
 */
@Service
public class SysPermissionService implements ISysPermissionService {
    @Autowired
    private IRoleService roleService;

    @Autowired
    private IResourceService resourceService;

    /**
     * 获取角色数据权限
     * 
     * @param user 用户信息
     * @return 角色权限信息
     */
    @Override
    public Set<String> getRolePermission(LoginUser user) {
        Set<String> roles = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin()) {
            roles.add("admin");
        }
        else {
            roles.addAll(roleService.selectRolePermissionByUserId(user.getId()));
        }
        return roles;
    }

    /**
     * 获取菜单数据权限
     * 
     * @param user 用户信息
     * @return 菜单权限信息
     */
    @Override
    public Set<String> getMenuPermission(LoginUser user) {
        Set<String> perms = new HashSet<String>();
        // 管理员拥有所有权限
        if (user.isAdmin())
        {
            perms.add("*:*:*");
        }
        else
        {
            perms.addAll(resourceService.selectResourcePermsByUserId(user.getId()));
        }
        return perms;
    }
}
