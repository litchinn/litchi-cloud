package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 角色机构
 * @Author: jeecg-boot
 * @Date:   2020-05-22
 * @Version: V1.0
 */
@Data
@TableName("role_organ")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="role_organ对象", description="角色机构")
public class RoleOrgan {
    
	/**角色ID*/
    @ApiModelProperty(value = "角色ID")
	private java.lang.Integer roleId;
	/**部门ID*/
    @ApiModelProperty(value = "部门ID")
	private java.lang.Integer organId;
}
