package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 角色
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Data
@TableName("role")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="role对象", description="角色")
public class Role {
    
	public Role(Integer roleId) {
		this.id = roleId;
	}
	
	public Role() {
		
	}
	
	/**角色ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "角色ID")
	private java.lang.Integer id;
	/**角色名称*/
    @ApiModelProperty(value = "角色名称")
	private java.lang.String name;
	/**角色权限字符串*/
    @ApiModelProperty(value = "角色权限字符串")
	private java.lang.String roleKey;
	/**显示顺序*/
    @ApiModelProperty(value = "显示顺序")
	private java.lang.Integer sort;
	/**数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）*/
    @ApiModelProperty(value = "数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
	private java.lang.String dataScope;
	/**角色状态（0正常 1停用）*/
    @ApiModelProperty(value = "角色状态（0正常 1停用）")
	private java.lang.String status;
	/**删除标志（0代表存在 2代表删除）*/
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
	private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
	private java.lang.String remark;
    
	public boolean isAdmin() {
		return isAdmin(this.id);
	}
	
	public boolean isAdmin(Integer id) {
		return id != null && 1 == id;
	}
}
