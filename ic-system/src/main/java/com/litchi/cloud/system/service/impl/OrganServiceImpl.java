package com.litchi.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.exception.CustomException;
import com.litchi.cloud.common.result.Result;
import com.litchi.cloud.system.entity.Organ;
import com.litchi.cloud.system.mapper.OrganMapper;
import com.litchi.cloud.system.service.IOrganService;
import com.litchi.cloud.system.vo.OrganVO;
import com.litchi.cloud.system.vo.TreeSelect;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wjhf
 * @since 2019-08-10
 */
@Service
public class OrganServiceImpl extends ServiceImpl<OrganMapper, Organ> implements IOrganService {

	//private static final int NAME_REPEAT = 1;
	//private static final int CODE_REPEAT = 2;

	@Resource
	private OrganMapper organMapper;

	@Override
	public Result<String> delete(Integer id) {
		organMapper.deleteById(id);
		return Result.ok("删除成功");
	}

	@Override
	public Result<OrganVO> getOrganById(Integer id) {
		OrganVO vo = new OrganVO();
		BeanUtils.copyProperties(organMapper.selectById(id), vo);
		return Result.ok(vo);
	}

	/**
	 * 构建前端所需要树结构
	 * 
	 * @param organs 部门列表
	 * @return 树结构列表
	 */
	public List<OrganVO> buildOrganTree(List<OrganVO> organs) {
		List<OrganVO> returnList = new ArrayList<OrganVO>();
		List<Integer> tempList = new ArrayList<Integer>();
		for (OrganVO organ : organs) {
			tempList.add(organ.getId());
		}
		for (Iterator<OrganVO> iterator = organs.iterator(); iterator.hasNext();) {
			OrganVO dept = (OrganVO) iterator.next();
			// 如果是顶级节点, 遍历该父节点的所有子节点
			if (!tempList.contains(dept.getParentId())) {
				recursionFn(organs, dept);
				returnList.add(dept);
			}
		}
		if (returnList.isEmpty()) {
			returnList = organs;
		}
		return returnList;
	}

	/**
	 * 构建前端所需要下拉树结构
	 * 
	 * @param depts 部门列表
	 * @return 下拉树结构列表
	 */
	@Override
	public List<TreeSelect> buildOrganTreeSelect(List<OrganVO> organs) {
		List<OrganVO> deptTrees = buildOrganTree(organs);
		return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
	}

	/**
	 * 根据角色ID查询部门树信息
	 * 
	 * @param roleId 角色ID
	 * @return 选中部门列表
	 */
	@Override
	public List<Integer> selectOrganListByRoleId(Integer roleId) {
		return organMapper.selectOrganListByRoleId(roleId);
	}

	/**
	 * 根据ID查询所有子部门（正常状态）
	 * 
	 * @param deptId 部门ID
	 * @return 子部门数
	 */
	@Override
	public int selectNormalChildrenOrganById(Integer deptId) {
		return organMapper.selectNormalChildrenOrganById(deptId);
	}

	/**
	 * 是否存在子节点
	 * 
	 * @param deptId 部门ID
	 * @return 结果
	 */
	@Override
	public boolean hasChildById(Integer id) {
		QueryWrapper<Organ> queryWrapper = new QueryWrapper<Organ>();
		queryWrapper.lambda().eq(Organ::getParentId, id).eq(Organ::getDelFlag, 0);
		int result = organMapper.selectCount(queryWrapper);
		return result > 0;
	}

	/**
	 * 查询部门是否存在用户
	 * 
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	@Override
	public boolean checkOrganExistUser(Integer id) {
		int result = organMapper.checkOrganExistUser(id);
		return result > 0 ? true : false;
	}

	/**
	 * 校验部门名称是否唯一
	 * 
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public String checkOrganNameUnique(Organ dept) {
		Long deptId = StringUtils.isEmpty(dept.getId()) ? -1L : dept.getId();
		Organ info = organMapper.checkOrganNameUnique(dept.getOrganName(), dept.getParentId());
		if (!StringUtils.isEmpty(info) && info.getId().longValue() != deptId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 新增保存部门信息
	 * 
	 * @param organ 部门信息
	 * @return 结果
	 */
	@Override
	public int insertOrgan(Organ organ) {
		Organ info = organMapper.selectById(organ.getParentId());
		// 如果父节点不为正常状态,则不允许新增子节点
		if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
			throw new CustomException("部门停用，不允许新增");
		}
		organ.setAncestors(info.getAncestors() + "," + organ.getParentId());
		return organMapper.insert(organ);
	}

	/**
	 * 修改保存部门信息
	 * 
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int updateOrgan(Organ dept) {
		Organ newParentDept = organMapper.selectById(dept.getParentId());
		Organ oldDept = organMapper.selectById(dept.getId());
		if (!StringUtils.isEmpty(newParentDept) && StringUtils.isEmpty(oldDept)) {
			String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getId();
			String oldAncestors = oldDept.getAncestors();
			dept.setAncestors(newAncestors);
			updateOrganChildren(dept.getId(), newAncestors, oldAncestors);
		}
		int result = organMapper.updateById(dept);
		if (UserConstants.DEPT_NORMAL.equals(dept.getStatus())) {
			// 如果该部门是启用状态，则启用该部门的所有上级部门
			updateParentOrganStatus(dept);
		}
		return result;
	}

	/**
	 * 修改该部门的父级部门状态
	 * 
	 * @param dept 当前部门
	 */
	private void updateParentOrganStatus(Organ dept) {
		String updateBy = dept.getUpdateBy();
		dept = organMapper.selectById(dept.getId());
		dept.setUpdateBy(updateBy);
		organMapper.updateOrganStatus(dept);
	}

	/**
	 * 修改子元素关系
	 * 
	 * @param deptId       被修改的部门ID
	 * @param newAncestors 新的父ID集合
	 * @param oldAncestors 旧的父ID集合
	 */
	public void updateOrganChildren(Integer deptId, String newAncestors, String oldAncestors) {
		List<Organ> children = organMapper.selectChildrenOrganById(deptId);
		for (Organ child : children) {
			child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
		}
		if (children.size() > 0) {
			organMapper.updateOrganChildren(children);
		}
	}

	/**
	 * 递归列表
	 */
	private void recursionFn(List<OrganVO> list, OrganVO t) {
		// 得到子节点列表
		List<OrganVO> childList = getChildList(list, t);
		t.setChildren(childList);
		for (OrganVO tChild : childList) {
			if (hasChild(list, tChild)) {
				// 判断是否有子节点
				Iterator<OrganVO> it = childList.iterator();
				while (it.hasNext()) {
					OrganVO n = (OrganVO) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private List<OrganVO> getChildList(List<OrganVO> list, OrganVO t) {
		List<OrganVO> tlist = new ArrayList<OrganVO>();
		Iterator<OrganVO> it = list.iterator();
		while (it.hasNext()) {
			OrganVO n = (OrganVO) it.next();
			if (!StringUtils.isEmpty(n.getParentId()) && n.getParentId().longValue() == t.getId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<OrganVO> list, OrganVO t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}

	@Override
	public int create(Organ organ) {
		Organ info = organMapper.selectById(organ.getParentId());
		// 如果父节点不为正常状态,则不允许新增子节点
		if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
			throw new CustomException("部门停用，不允许新增");
		}
		organ.setAncestors(info.getAncestors() + "," + organ.getParentId());
		return organMapper.insert(organ);
	}
}
