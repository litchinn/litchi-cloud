package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 资源菜单
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Data
@TableName("resource")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="resource对象", description="资源菜单")
public class Resource {
    
	/**菜单ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "菜单ID")
	private java.lang.Integer id;
	/**菜单名称*/
    @ApiModelProperty(value = "菜单名称")
	private java.lang.String name;
	/**父菜单ID*/
    @ApiModelProperty(value = "父菜单ID")
	private java.lang.Integer parentId;
	/**显示顺序*/
    @ApiModelProperty(value = "显示顺序")
	private java.lang.Integer orderNum;
	/**路由地址*/
    @ApiModelProperty(value = "路由地址")
	private java.lang.String path;
	/**组件路径*/
    @ApiModelProperty(value = "组件路径")
	private java.lang.String component;
	/**是否为外链（0是 1否）*/
    @ApiModelProperty(value = "是否为外链（0是 1否）")
	private java.lang.Integer isFrame;
	/**菜单类型（M目录 C菜单 F按钮）*/
    @ApiModelProperty(value = "菜单类型（M目录 C菜单 F按钮）")
	private java.lang.String menuType;
	/**菜单状态（0显示 1隐藏）*/
    @ApiModelProperty(value = "菜单状态（0显示 1隐藏）")
	private java.lang.String visible;
	/**菜单状态（0正常 1停用）*/
    @ApiModelProperty(value = "菜单状态（0正常 1停用）")
	private java.lang.String status;
	/**权限标识*/
    @ApiModelProperty(value = "权限标识")
	private java.lang.String perms = "";
	/**菜单图标*/
    @ApiModelProperty(value = "菜单图标")
	private java.lang.String icon;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
	private java.lang.String remark;
}
