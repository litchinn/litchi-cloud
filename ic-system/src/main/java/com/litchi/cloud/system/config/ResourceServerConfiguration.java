package com.litchi.cloud.system.config;

import javax.servlet.http.HttpServletResponse;

import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import feign.RequestInterceptor;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月10日 上午9:28:37
 * @vesion: 1.0
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers(
				"/webjars/**",
          		"/resources/**",
          		"/doc.html",
          		"/swagger-resources/**",
            	"/v2/api-docs")
			.permitAll()
			.anyRequest().authenticated()
			.and()
			.httpBasic();
	}
}