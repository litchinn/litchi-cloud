package com.litchi.cloud.system.controller;

import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.system.entity.SysNotice;
import com.litchi.cloud.system.service.ISysNoticeService;

/**
 * 公告 信息操作处理
 * 
 * @author: wjhf@litchi
 * @date: 2020年5月27日 下午3:46:14
 * @vesion: 1.0
 */
@RestController
@RequestMapping("/notice")
public class SysNoticeController {
	
	@Autowired
	private ISysNoticeService noticeService;

	/**
	 * 获取通知公告列表
	 */
	@GetMapping("/list")
	public PageResult<SysNotice> list(SysNotice notice,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  HttpServletRequest req) {
		QueryWrapper<SysNotice> queryWrapper = new QueryWrapper<SysNotice>();
		queryWrapper.lambda()
			.like(!StringUtils.isEmpty(notice.getNoticeTitle()), SysNotice::getNoticeTitle, notice.getNoticeTitle())
			.eq(!StringUtils.isEmpty(notice.getNoticeType()), SysNotice::getNoticeType, notice.getNoticeType())
			.like(!StringUtils.isEmpty(notice.getCreateBy()), SysNotice::getCreateBy, notice.getCreateBy());
		Page<SysNotice> page = new Page<SysNotice>(pageNo, pageSize);
		Page<SysNotice> list = noticeService.page(page, queryWrapper);
		return new PageResult<SysNotice>(list.getRecords(), list.getTotal());
	}

	/**
	 * 根据通知公告编号获取详细信息
	 */
	@GetMapping(value = "/{noticeId}")
	public AjaxResult getInfo(@PathVariable Long noticeId) {
		return AjaxResult.success(noticeService.getById(noticeId));
	}

	/**
	 * 新增通知公告
	 */
	@PostMapping
	public AjaxResult add(@Validated @RequestBody SysNotice notice) {
		notice.setCreateBy(WebContextUtil.getUsername());
		notice.setCreateTime(new Date());
		noticeService.save(notice);
		return AjaxResult.success();
	}

	/**
	 * 修改通知公告
	 */
	@PutMapping
	public AjaxResult edit(@Validated @RequestBody SysNotice notice) {
		notice.setUpdateBy(WebContextUtil.getUsername());
		notice.setUpdateTime(new Date());
		noticeService.updateById(notice);
		return AjaxResult.success();
	}

	/**
	 * 删除通知公告
	 */
	@DeleteMapping("/{noticeIds}")
	public AjaxResult remove(@PathVariable Integer[] noticeIds) {
		noticeService.removeByIds(Arrays.asList(noticeIds));
		return AjaxResult.success();
	}
}
