package com.litchi.cloud.system.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.system.entity.Organ;
import com.litchi.cloud.system.service.IOrganService;
import com.litchi.cloud.system.vo.OrganVO;

import io.swagger.annotations.Api;

/**
 * 类说明
 * 
 * @author: wjhf@litchi
 * @date: 2020年5月13日 下午3:52:00
 * @vesion: 1.0
 */
@RestController
@RequestMapping("/organ")
@Api(tags = { "机构管理" })
public class OrganController {
	@Autowired
	private IOrganService organService;

	/**
	 * 获取部门列表
	 */
	@GetMapping("/list")
	public AjaxResult list(Organ organ) {
		QueryWrapper<Organ> queryWrapper = new QueryWrapper<Organ>();
		queryWrapper.lambda()
				.like(!StringUtils.isEmpty(organ.getOrganName()), Organ::getOrganName, organ.getOrganName())
				.eq(!StringUtils.isEmpty(organ.getStatus()), Organ::getStatus, organ.getStatus());
		List<Organ> organs = organService.list(queryWrapper);
		return AjaxResult.success(organs);
	}

	/**
	 * 根据部门编号获取详细信息
	 */
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable Integer id) {
		return AjaxResult.success(organService.getById(id));
	}

	/**
	 * 获取部门下拉树列表
	 */
	@GetMapping("/treeselect")
	public AjaxResult treeselect(Organ organ) {
		QueryWrapper<Organ> queryWrapper = new QueryWrapper<Organ>();
		queryWrapper.lambda().like(!StringUtils.isEmpty(organ.getOrganName()), Organ::getOrganName,
				organ.getOrganName());
		List<Organ> organs = organService.list(queryWrapper);
		List<OrganVO> vos = new ArrayList<OrganVO>();
		for (Organ o : organs) {
			OrganVO vo = new OrganVO();
			BeanUtils.copyProperties(o, vo);
			vos.add(vo);
		}
		return AjaxResult.success(organService.buildOrganTreeSelect(vos));
	}

	/**
	 * 加载对应角色部门列表树
	 */
	@GetMapping(value = "/roleOrganTreeselect/{roleId}")
	public AjaxResult roleDeptTreeselect(@PathVariable("roleId") Integer roleId) {
		List<Organ> organs = organService.list(new QueryWrapper<Organ>());
		List<OrganVO> vos = new ArrayList<OrganVO>();
		for (Organ o : organs) {
			OrganVO vo = new OrganVO();
			BeanUtils.copyProperties(o, vo);
			vos.add(vo);
		}
		AjaxResult ajax = AjaxResult.success();
		ajax.put("checkedKeys", organService.selectOrganListByRoleId(roleId));
		ajax.put("depts", organService.buildOrganTreeSelect(vos));
		return ajax;
	}

	/**
	 * 新增部门
	 */
	@PostMapping
	public AjaxResult add(@Validated @RequestBody Organ organ) {
		if (UserConstants.NOT_UNIQUE.equals(organService.checkOrganNameUnique(organ))) {
			return AjaxResult.error("新增部门'" + organ.getOrganName() + "'失败，部门名称已存在");
		}
		organ.setCreateBy(WebContextUtil.getUsername());
		organ.setCreateTime(new Date());
		organService.create(organ);
		return AjaxResult.success();
	}

	/**
	 * 修改部门
	 */
	@PutMapping
	public AjaxResult edit(@Validated @RequestBody Organ organ) {
		if (UserConstants.NOT_UNIQUE.equals(organService.checkOrganNameUnique(organ))) {
			return AjaxResult.error("修改部门'" + organ.getOrganName() + "'失败，部门名称已存在");
		} else if (organ.getParentId().equals(organ.getId())) {
			return AjaxResult.error("修改部门'" + organ.getOrganName() + "'失败，上级部门不能是自己");
		} else if (UserConstants.DEPT_DISABLE.equals(organ.getStatus())
				&& organService.selectNormalChildrenOrganById(organ.getId()) > 0) {
			return AjaxResult.error("该部门包含未停用的子部门！");
		}
		organ.setUpdateBy(WebContextUtil.getUsername());
		organ.setUpdateTime(new Date());
		organService.updateOrgan(organ);
		return AjaxResult.success();
	}

	/**
	 * 删除部门
	 */
	@DeleteMapping("/{id}")
	public AjaxResult remove(@PathVariable Integer id) {
		if (organService.hasChildById(id)) {
			return AjaxResult.error("存在下级部门,不允许删除");
		}
		if (organService.checkOrganExistUser(id)) {
			return AjaxResult.error("部门存在用户,不允许删除");
		}
		organService.removeById(id);
		return AjaxResult.success();
	}

	/**
	 * 响应返回结果
	 * 
	 * @param rows 影响行数
	 * @return 操作结果
	 */
	protected AjaxResult toAjax(int rows) {
		return rows > 0 ? AjaxResult.success() : AjaxResult.error();
	}
}
