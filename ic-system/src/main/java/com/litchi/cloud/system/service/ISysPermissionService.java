package com.litchi.cloud.system.service;

import java.util.Set;

import com.litchi.cloud.common.beans.LoginUser;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月14日 下午4:52:42
 * @vesion: 1.0
 */
public interface ISysPermissionService {

	Set<String> getRolePermission(LoginUser uu);

	Set<String> getMenuPermission(LoginUser uu);

}
