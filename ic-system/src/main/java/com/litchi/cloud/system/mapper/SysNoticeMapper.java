package com.litchi.cloud.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.SysNotice;

/**
 * @Description: 通知公告
 * @Author: jeecg-boot
 * @Date:   2020-05-27
 * @Version: V1.0
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {

}
