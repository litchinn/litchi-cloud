package com.litchi.cloud.system.vo;

import java.util.ArrayList;
import java.util.List;

import com.litchi.cloud.system.entity.Organ;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * 机构VO类
 * @author: wjhf-litchi
 * @date: 2019年8月11日 下午7:12:44
 * @vesion: 0.0.1
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel
public class OrganVO extends Organ{
    
	@ApiModelProperty(value="子机构")
    private List<OrganVO> children = new ArrayList<OrganVO>();
}
