package com.litchi.cloud.system.vo;

import java.util.ArrayList;
import java.util.List;

import com.litchi.cloud.system.entity.Resource;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * 类说明
 * @author: wjhf-litchi
 * @date: 2019年8月12日 上午9:13:42
 * @vesion: 0.0.1
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ResourceVO extends Resource{

    /** 子菜单 */
    @ApiModelProperty(value = "子菜单")
    private List<ResourceVO> children = new ArrayList<ResourceVO>();
}
