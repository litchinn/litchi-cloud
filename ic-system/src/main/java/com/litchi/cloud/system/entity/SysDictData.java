package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;

import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 字典数据
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("sys_dict_data")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="sys_dict_data对象", description="字典数据")
public class SysDictData {
    
	/**字典编码*/
    @ApiModelProperty(value = "字典编码")
    @Excel(name = "字典编码", width = 15)
	private java.lang.Integer dictCode;
	/**字典排序*/
    @ApiModelProperty(value = "字典排序")
	private java.lang.Integer dictSort;
	/**字典标签*/
    @ApiModelProperty(value = "字典标签")
    @Excel(name = "字典标签", width = 15)
	private java.lang.String dictLabel;
	/**字典键值*/
    @ApiModelProperty(value = "字典键值")
    @Excel(name = "字典键值", width = 15)
	private java.lang.String dictValue;
	/**字典类型*/
    @ApiModelProperty(value = "字典类型")
    @Excel(name = "字典类型", width = 15)
	private java.lang.String dictType;
	/**样式属性（其他样式扩展）*/
    @ApiModelProperty(value = "样式属性（其他样式扩展）")
	private java.lang.String cssClass;
	/**表格回显样式*/
    @ApiModelProperty(value = "表格回显样式")
	private java.lang.String listClass;
	/**是否默认（Y是 N否）*/
    @ApiModelProperty(value = "是否默认（Y是 N否）")
    @Excel(name = "是否默认", width = 15)
	private java.lang.String isDefault;
	/**状态（0正常 1停用）*/
    @ApiModelProperty(value = "状态（0正常 1停用）")
    @Excel(name = "状态", width = 15)
	private java.lang.String status;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    @Excel(name = "创建者", width = 15)
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
    @Excel(name = "更新者", width = 15)
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    @Excel(name = "更新时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
    @Excel(name = "备注", width = 15)
	private java.lang.String remark;
}
