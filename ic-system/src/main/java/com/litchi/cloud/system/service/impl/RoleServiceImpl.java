package com.litchi.cloud.system.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.exception.CustomException;
import com.litchi.cloud.system.entity.Role;
import com.litchi.cloud.system.entity.RoleOrgan;
import com.litchi.cloud.system.entity.RoleResource;
import com.litchi.cloud.system.entity.UserRole;
import com.litchi.cloud.system.mapper.RoleMapper;
import com.litchi.cloud.system.mapper.RoleOrganMapper;
import com.litchi.cloud.system.mapper.RoleResourceMapper;
import com.litchi.cloud.system.mapper.UserRoleMapper;
import com.litchi.cloud.system.service.IRoleService;
import com.litchi.cloud.system.vo.RoleVO;

/**
 * @Description: 角色
 * @Author: jeecg-boot
 * @Date: 2020-05-13
 * @Version: V1.0
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

	@Autowired
	private RoleMapper roleMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private RoleResourceMapper roleResourceMapper;
	@Autowired
	private RoleOrganMapper roleOrganMapper;

	@Override
	public Set<String> selectRolePermissionByUserId(Integer id) {
		List<Role> perms = roleMapper.selectRolePermissionByUserId(id);
		Set<String> permsSet = new HashSet<>();
		for (Role perm : perms) {
			if (!StringUtils.isEmpty(perm)) {
				permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
			}
		}
		return permsSet;
	}

	@Override
	public Role selectRoleById(Integer id) {
		return roleMapper.selectRoleById(id);
	}

	/**
	 * 校验角色名称是否唯一
	 * 
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	public String checkRoleNameUnique(Role role) {
		Integer roleId = StringUtils.isEmpty(role.getId()) ? -1 : role.getId();
		QueryWrapper<Role> queryWrapper = new QueryWrapper<Role>();
		queryWrapper.lambda().eq(Role::getName, role.getName());
		Role info = roleMapper.selectOne(queryWrapper);
		if (!StringUtils.isEmpty(info) && info.getId().longValue() != roleId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验角色权限是否唯一
	 * 
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	public String checkRoleKeyUnique(Role role) {
		Integer roleId = StringUtils.isEmpty(role.getId()) ? -1 : role.getId();
		QueryWrapper<Role> queryWrapper = new QueryWrapper<Role>();
		queryWrapper.lambda().eq(Role::getRoleKey, role.getRoleKey());
		Role info = roleMapper.selectOne(queryWrapper);
		if (!StringUtils.isEmpty(info) && info.getId() != roleId) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	@Override
	public void deleteByIds(List<Integer> roleIds) {
		for (Integer roleId : roleIds) {
			checkRoleAllowed(new Role(roleId));
			Role role = selectRoleById(roleId);
			if (countUserRoleByRoleId(roleId) > 0) {
				throw new CustomException(String.format("%1$s已分配,不能删除", role.getName()));
			}
		}
		this.baseMapper.deleteBatchIds(roleIds);
	}

	private int countUserRoleByRoleId(Integer roleId) {
		QueryWrapper<UserRole> queryWrapper = new QueryWrapper<UserRole>();
		queryWrapper.lambda().eq(UserRole::getRoleId, roleId);
		return userRoleMapper.selectCount(queryWrapper);
	}

	@Override
	public void checkRoleAllowed(Role role) {
		if (!StringUtils.isEmpty(role.getId()) && role.isAdmin()) {
			throw new CustomException("不允许操作超级管理员角色");
		}
	}

	/**
	 * 修改保存角色信息
	 * 
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateRole(RoleVO role) {
		// 修改角色信息
		roleMapper.updateById(role);
		// 删除角色与菜单关联
		QueryWrapper<RoleResource> queryWrapper = new QueryWrapper<RoleResource>();
		queryWrapper.lambda().eq(RoleResource::getRoleId, role.getId());
		roleResourceMapper.delete(queryWrapper);
		return insertRoleResource(role);
	}

	/**
	 * 新增角色菜单信息
	 * 
	 * @param role 角色对象
	 */
	@Override
	public int insertRoleResource(RoleVO role) {
		// 新增用户与角色管理
		for (Integer resourceId : role.getResourceIds()) {
			RoleResource rm = new RoleResource();
			rm.setRoleId(role.getId());
			rm.setResourceId(resourceId);
			roleResourceMapper.insert(rm);
		}
		return role.getResourceIds().length;
	}

	/**
	 * 修改数据权限信息
	 * 
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int authDataScope(RoleVO role) {
		// 修改角色信息
		roleMapper.updateById(role);
		// 删除角色与部门关联
		QueryWrapper<RoleOrgan> queryWrapper = new QueryWrapper<RoleOrgan>();
		queryWrapper.lambda().eq(RoleOrgan::getRoleId, role.getId());
		roleOrganMapper.delete(queryWrapper);
		// 新增角色和部门信息（数据权限）
		return insertRoleOrgan(role);
	}

	private int insertRoleOrgan(RoleVO role) {
		int rows = 1;
		// 新增角色与部门（数据权限）管理
		for (Integer deptId : role.getOrganIds()) {
			RoleOrgan rd = new RoleOrgan();
			rd.setRoleId(role.getId());
			rd.setOrganId(deptId);
			roleOrganMapper.insert(rd);
			rows = rows + 1;
		}
		return rows;
	}

	@Override
	public List<Role> selectAll() {
		return roleMapper.selectRoleList(new Role());
	}

}
