package com.litchi.cloud.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.SysConfig;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
