package com.litchi.cloud.system.service;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.Resource;
import com.litchi.cloud.system.vo.ResourceVO;
import com.litchi.cloud.system.vo.RouterVO;
import com.litchi.cloud.system.vo.TreeSelect;

/**
 * @Description: 资源菜单
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface IResourceService extends IService<Resource> {

	/** 
	 * 获取资源菜单树
	 * @param userId
	 * @return 
	 * 返回类型  List<ResourceVO> 
	 */
	List<ResourceVO> selectMenuTreeByUserId(Integer userId);

	/** 
	 * 构建菜单
	 * @param menus
	 * @return 
	 * 返回类型  String 
	 */
	List<RouterVO> buildMenus(List<ResourceVO> menus);

	Set<String> selectResourcePermsByUserId(Integer id);

	List<TreeSelect> buildResourceTreeSelect(List<ResourceVO> menus);

	List<ResourceVO> selectResourceList(Integer userId);

	List<ResourceVO> selectResourceList(Resource menu, Integer userId);

	List<Integer> selectResourceListByRoleId(Integer roleId);

	String checkResourceNameUnique(ResourceVO resource);

	boolean hasChildByResourceId(Integer id);

	boolean checkResourceExistRole(Integer id);

}
