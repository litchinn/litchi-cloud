package com.litchi.cloud.system.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.system.entity.SysDictType;
import com.litchi.cloud.system.service.ISysDictTypeService;

/**
 * 类说明
 * 
 * @author: wjhf@litchi
 * @date: 2020年5月22日 下午5:04:28
 * @vesion: 1.0
 */
@RestController
@RequestMapping("/dict/type")
public class SysDictTypeController {

	@Autowired
	private ISysDictTypeService dictTypeService;

	@GetMapping("/list")
	public PageResult<SysDictType> list(SysDictType dictType,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "beginTime", required = false) String startTime,
			@RequestParam(name = "endTime", required = false) String endTime, HttpServletRequest req) {
		Page<SysDictType> page = new Page<SysDictType>(pageNo, pageSize);
		QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<SysDictType>();
		queryWrapper.lambda()
				.eq(!StringUtils.isEmpty(dictType.getStatus()), SysDictType::getStatus, dictType.getStatus())
				.like(!StringUtils.isEmpty(dictType.getDictName()), SysDictType::getDictName, dictType.getDictName())
				.like(!StringUtils.isEmpty(dictType.getDictType()), SysDictType::getDictType, dictType.getDictType())
				.between(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime), SysDictType::getCreateTime,
						startTime, endTime);
		Page<SysDictType> list = dictTypeService.page(page, queryWrapper);
		return new PageResult<SysDictType>(list.getRecords(), list.getTotal());
	}

	/**
	 * 查询字典类型详细
	 */
	@GetMapping(value = "/{dictId}")
	public AjaxResult getInfo(@PathVariable Long dictId) {
		return AjaxResult.success(dictTypeService.getById(dictId));
	}

	/**
	 * 新增字典类型
	 */
	@PostMapping
	public AjaxResult add(@Validated @RequestBody SysDictType dict) {
		if (UserConstants.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict))) {
			return AjaxResult.error("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
		}
		dict.setCreateBy(WebContextUtil.getUsername());
		dict.setCreateTime(new Date());
		dictTypeService.save(dict);
		return AjaxResult.success();
	}

	/**
	 * 修改字典类型
	 */
	@PutMapping
	public AjaxResult edit(@Validated @RequestBody SysDictType dict) {
		if (UserConstants.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict))) {
			return AjaxResult.error("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
		}
		dict.setUpdateBy(WebContextUtil.getUsername());
		dictTypeService.updateById(dict);
		return AjaxResult.success();
	}

	/**
	 * 删除字典类型
	 */
	@DeleteMapping("/{dictIds}")
	public AjaxResult remove(@PathVariable Integer[] dictIds) {
		dictTypeService.removeByIds(Arrays.asList(dictIds));
		return AjaxResult.success();
	}

	/**
	 * 获取字典选择框列表
	 */
	@GetMapping("/optionselect")
	public AjaxResult optionselect() {
		List<SysDictType> dictTypes = dictTypeService.list();
		return AjaxResult.success(dictTypes);
	}
}
