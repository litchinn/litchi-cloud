package com.litchi.cloud.system.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.system.entity.SysDictData;
import com.litchi.cloud.system.service.ISysDictDataService;

/** 
 * 字典数据
 * @author: wjhf@litchi 
 * @date: 2020年5月14日 上午11:33:51
 * @vesion: 1.0
 */
@RestController
@RequestMapping("/dict/data")
public class SysDictDataController {
	
    @Autowired
    private ISysDictDataService dictDataService;

    @GetMapping("/list")
    public PageResult<SysDictData> list(SysDictData dictData,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  @RequestParam(name="startTime", required = false) String startTime,
			  @RequestParam(name="endTime", required = false) String endTime,
			  HttpServletRequest req) {
    	QueryWrapper<SysDictData> queryWrapper = new QueryWrapper<SysDictData>();
    	queryWrapper.lambda()
    		.between(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime), SysDictData::getCreateTime, startTime, endTime)
    		.eq(!StringUtils.isEmpty(dictData.getStatus()), SysDictData::getStatus, dictData.getStatus())
    		.eq(!StringUtils.isEmpty(dictData.getDictLabel()), SysDictData::getDictLabel, dictData.getDictLabel())
    		.eq(!StringUtils.isEmpty(dictData.getDictType()), SysDictData::getDictType, dictData.getDictType());
    	Page<SysDictData> page = new Page<SysDictData>(pageNo, pageSize);
    	IPage<SysDictData> list = dictDataService.page(page, queryWrapper);
        return new PageResult<SysDictData>(list.getRecords(), list.getTotal());
    }

    @GetMapping("/export")
    public ModelAndView export(SysDictData dictData,
			  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
			  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
			  @RequestParam(name="startTime", required = false) String startTime,
			  @RequestParam(name="endTime", required = false) String endTime,
			  HttpServletRequest req) {
    	String userName = WebContextUtil.getUsername();
    	QueryWrapper<SysDictData> queryWrapper = new QueryWrapper<SysDictData>();
    	queryWrapper.lambda()
			.between(!StringUtils.isEmpty(startTime) && !StringUtils.isEmpty(endTime), SysDictData::getCreateTime, startTime, endTime)
			.eq(!StringUtils.isEmpty(dictData.getDictLabel()), SysDictData::getDictLabel, dictData.getDictLabel())
			.eq(!StringUtils.isEmpty(dictData.getDictType()), SysDictData::getDictType, dictData.getDictType());
        //Step.2 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<SysDictData> pageList = dictDataService.list(queryWrapper);
        //导出文件名称
        mv.addObject(NormalExcelConstants.FILE_NAME, "字典数据列表");
        mv.addObject(NormalExcelConstants.CLASS, SysDictData.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("字典数据列表数据", "导出人:" + userName, "字典数据到处"));
        mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
        return mv;
    }

    /**
     * 查询字典数据详细
     */
    @GetMapping(value = "/{dictCode}")
    public AjaxResult getInfo(@PathVariable Integer dictCode) {
        return AjaxResult.success(dictDataService.getById(dictCode));
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    @GetMapping(value = "/dictType/{dictType}")
    public AjaxResult dictType(@PathVariable String dictType) {
    	QueryWrapper<SysDictData> queryWrapper = new QueryWrapper<SysDictData>();
    	queryWrapper.lambda().eq(SysDictData::getDictType, dictType);
        return AjaxResult.success(dictDataService.list(queryWrapper));
    }

    /**
     * 新增字典类型
     */
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysDictData dict) {
        dict.setCreateBy(WebContextUtil.getUsername());
        dictDataService.save(dict);
        return AjaxResult.success();
    }

    /**
     * 修改保存字典类型
     */
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysDictData dict) {
        dict.setUpdateBy(WebContextUtil.getUsername());
        dictDataService.updateById(dict);
        return AjaxResult.success();
    }

    /**
     * 删除字典类型
     */
    @DeleteMapping("/{dictCodes}")
    public AjaxResult remove(@PathVariable Integer[] dictCodes) {
    	dictDataService.removeByIds(Arrays.asList(dictCodes));
        return AjaxResult.success();
    }
}
