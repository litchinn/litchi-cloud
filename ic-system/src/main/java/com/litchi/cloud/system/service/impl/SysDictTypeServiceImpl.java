package com.litchi.cloud.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.system.entity.SysDictType;
import com.litchi.cloud.system.mapper.SysDictTypeMapper;
import com.litchi.cloud.system.service.ISysDictTypeService;

/**
 * @Description: 字典类型
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictType> implements ISysDictTypeService {

	 /**
     * 校验字典类型称是否唯一
     * 
     * @param dict 字典类型
     * @return 结果
     */
    @Override
    public String checkDictTypeUnique(SysDictType dict)
    {
        Integer dictId = StringUtils.isEmpty(dict.getDictId()) ? -1 : dict.getDictId();
        QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<SysDictType>();
        queryWrapper.lambda().eq(SysDictType::getDictType, dict.getDictType());
        SysDictType dictType = this.baseMapper.selectOne(queryWrapper);
        if (!StringUtils.isEmpty(dictType) && dictType.getDictId().intValue() != dictId.intValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
