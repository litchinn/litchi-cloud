package com.litchi.cloud.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.Resource;
import com.litchi.cloud.system.entity.Role;

/**
 * @Description: 资源菜单
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface ResourceMapper extends BaseMapper<Resource> {

	List<Resource> selectMenuTreeByUserId(Integer userId);

	List<String> selectMenuPermsByUserId(Integer userd);

	List<Resource> selectResourceListByUserId(@Param("menu") Resource menu, @Param("userId") Integer userId);

	List<Integer> selectResourceListByRoleId(Integer roleId);

	List<String> selectPermsByRoles(@Param("roles") List<Integer> roles);

}
