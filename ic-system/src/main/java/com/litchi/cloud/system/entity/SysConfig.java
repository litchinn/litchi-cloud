package com.litchi.cloud.system.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("sys_config")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="sys_config对象", description="系统参数")
public class SysConfig {
    
	/**参数主键*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "参数主键")
	private java.lang.Integer configId;
	/**参数名称*/
    @ApiModelProperty(value = "参数名称")
	private java.lang.String configName;
	/**参数键名*/
    @ApiModelProperty(value = "参数键名")
	private java.lang.String configKey;
	/**参数键值*/
    @ApiModelProperty(value = "参数键值")
	private java.lang.String configValue;
	/**系统内置（Y是 N否）*/
    @ApiModelProperty(value = "系统内置（Y是 N否）")
	private java.lang.String configType;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
	private java.lang.String remark;
}
