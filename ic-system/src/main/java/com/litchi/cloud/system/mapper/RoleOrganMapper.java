package com.litchi.cloud.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.RoleOrgan;

/**
 * @Description: 角色机构
 * @Author: jeecg-boot
 * @Date:   2020-05-22
 * @Version: V1.0
 */
public interface RoleOrganMapper extends BaseMapper<RoleOrgan> {

}
