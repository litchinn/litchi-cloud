package com.litchi.cloud.system.controller;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.litchi.cloud.common.beans.UserDetailBean;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.service.IUserService;
import com.litchi.cloud.common.utils.WebContextUtil;
import com.litchi.cloud.common.vo.User;

/**
 * 个人信息
 * 
 * @author: wjhf@litchi
 * @date: 2020年5月27日 下午4:04:56
 * @vesion: 1.0
 */
@RestController
@RequestMapping("/user/profile")
public class SysProfileController {
	
	@Autowired
	private IUserService userService;

	/**
	 * 个人信息
	 */
	@GetMapping
	public AjaxResult profile() {
		String username = WebContextUtil.getCurrUsername();
		AjaxResult ajax = AjaxResult.success(username);
		ajax.put("roleGroup", userService.selectUserRoleGroup(username));
		return ajax;
	}

	/**
	 * 修改用户
	 */
	@PutMapping
	public AjaxResult updateProfile(@RequestBody User user) {
		if (userService.updateById(user)) {
			return AjaxResult.success();
		}
		return AjaxResult.error("修改个人信息异常，请联系管理员");
	}

	/**
	 * 重置密码
	 */
	@PutMapping("/updatePwd")
	public AjaxResult updatePwd(String oldPassword, String newPassword) {
		Map<String, Object> user = WebContextUtil.getCurrPrincipal();
		String userName = user.get("username").toString();
		User u = userService.getById(user.get("id").toString());
		String password = u.getPassword();
		if (!WebContextUtil.matchesPassword(oldPassword, password)) {
			return AjaxResult.error("修改密码失败，旧密码错误");
		}
		if (WebContextUtil.matchesPassword(newPassword, password)) {
			return AjaxResult.error("新密码不能与旧密码相同");
		}
		UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>();
		updateWrapper.lambda().eq(User::getUserName, userName);
		User uu = new User();
		uu.setPassword(WebContextUtil.encryptPassword(newPassword));
		if (userService.update(uu, updateWrapper)) {
			return AjaxResult.success();
		}
		return AjaxResult.error("修改密码异常，请联系管理员");
	}

	/**
	 * 头像上传
	 */
	@PostMapping("/avatar")
	public AjaxResult avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException {
		if (!file.isEmpty()) {
//			String avatar = FileUploadUtils.upload(RuoYiConfig.getAvatarPath(), file);
//			if (userService.updateUserAvatar(loginUser.getUsername(), avatar)) {
//				AjaxResult ajax = AjaxResult.success();
//				ajax.put("imgUrl", avatar);
//				// 更新缓存用户头像
//				loginUser.getUser().setAvatar(avatar);
//				tokenService.setLoginUser(loginUser);
				return AjaxResult.success();
//			}
		}
		return AjaxResult.error("上传图片异常，请联系管理员");
	}
}