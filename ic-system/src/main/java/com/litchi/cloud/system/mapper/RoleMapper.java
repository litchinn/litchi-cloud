package com.litchi.cloud.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litchi.cloud.system.entity.Role;

/**
 * @Description: 角色
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
public interface RoleMapper extends BaseMapper<Role> {

	List<Role> selectRolePermissionByUserId(Integer userId);

	Role selectRoleById(Integer id);

	List<Role> selectRoleList(Role role);

	List<Role> selectRolesByUserName(String userName);

}
