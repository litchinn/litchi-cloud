package com.litchi.cloud.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.system.entity.SysConfig;

/**
 * @Description: 系统参数
 * @Author: jeecg-boot
 * @Date:   2020-05-14
 * @Version: V1.0
 */
public interface ISysConfigService extends IService<SysConfig> {

	String checkConfigKeyUnique(SysConfig config);

}
