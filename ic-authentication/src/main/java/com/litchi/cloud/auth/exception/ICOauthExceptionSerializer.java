package com.litchi.cloud.auth.exception;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.litchi.cloud.common.enums.ErrorCodeEnum;

import lombok.SneakyThrows;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:39:51
 * @vesion: 1.0
 */
public class ICOauthExceptionSerializer extends StdSerializer<ICOauthException> {
	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = -7194195218829747452L;

	public ICOauthExceptionSerializer() {
		super(ICOauthException.class);
	}

	@Override
	@SneakyThrows
	public void serialize(ICOauthException value, JsonGenerator gen, SerializerProvider provider) {
		gen.writeStartObject();
		gen.writeObjectField("code", ErrorCodeEnum.BUSINESS_FAIL.getCode());
		gen.writeStringField("msg", value.getMessage());
		gen.writeStringField("data", value.getErrorCode());
		gen.writeEndObject();
	}
}
