package com.litchi.cloud.auth.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:50:18
 * @vesion: 1.0
 */
@JsonSerialize(using = ICOauthExceptionSerializer.class)
public class InvalidException extends ICOauthException {

	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = -8204800398307906203L;

	public InvalidException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "invalid_exception";
	}

	@Override
	public int getHttpErrorCode() {
		return 426;
	}

}
