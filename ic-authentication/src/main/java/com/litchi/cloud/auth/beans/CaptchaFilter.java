package com.litchi.cloud.auth.beans;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.litchi.cloud.common.constants.Oauth2Constant;
import com.litchi.cloud.common.exception.CustomOauthException;
import com.litchi.cloud.common.utils.RedisCache;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月11日 下午5:29:00
 * @vesion: 1.0
 */
@Component
public class CaptchaFilter extends GenericFilterBean {
    private String defaultFilterProcessUrl = "/oauth/token";
    
    @Autowired
    private RedisCache redisCache;

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        if("password".equals(request.getParameter("grant_type"))) {
            if ("POST".equalsIgnoreCase(request.getMethod()) && defaultFilterProcessUrl.equals(request.getServletPath())) {
                // 验证码验证
                String requestCaptcha = request.getParameter("code");
                String verifyKey = Oauth2Constant.CAPTCHA_CODE_KEY + request.getParameter("uuid");
                String genCaptcha = redisCache.getCacheObject(verifyKey);
                if(StringUtils.isEmpty(genCaptcha)) {
                	throw new CustomOauthException("验证码已过期");
                }
                if (StringUtils.isEmpty(requestCaptcha))
                    throw new CustomOauthException("验证码不能为空!");
                if (!genCaptcha.toLowerCase().equals(requestCaptcha.toLowerCase())) {
                    throw new CustomOauthException("验证码错误!");
                }
            }
        }
        chain.doFilter(request, response);
    }
}
