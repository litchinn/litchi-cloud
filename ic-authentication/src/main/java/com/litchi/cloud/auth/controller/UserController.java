package com.litchi.cloud.auth.controller;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.litchi.cloud.common.beans.LoginUser;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月10日 上午8:54:55
 * @vesion: 1.0
 */
@RestController
public class UserController {

	@GetMapping("/userinfo")
	public Map<String, Object> currentUser(Principal user) {
		OAuth2Authentication a = (OAuth2Authentication) user;
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("user", a.getPrincipal());
		return map;
	}
}
