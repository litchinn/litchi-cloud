package com.litchi.cloud.auth.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:52:21
 * @vesion: 1.0
 */
@JsonSerialize(using = ICOauthExceptionSerializer.class)
public class MethodNotAllowed extends ICOauthException {

	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = 1788543943604429789L;

	public MethodNotAllowed(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "method_not_allowed";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.METHOD_NOT_ALLOWED.value();
	}
}
