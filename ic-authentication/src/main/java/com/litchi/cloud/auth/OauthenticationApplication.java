package com.litchi.cloud.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 下午3:06:25
 * @vesion: 1.0
 */
@EnableFeignClients
@SpringBootApplication(scanBasePackages = { "com.litchi.cloud.common", "com.litchi.cloud.auth" })
@EnableDiscoveryClient
public class OauthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthenticationApplication.class, args);
	}

}
