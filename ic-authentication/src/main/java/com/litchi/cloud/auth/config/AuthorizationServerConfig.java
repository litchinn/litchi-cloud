package com.litchi.cloud.auth.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import com.litchi.cloud.auth.service.SecurityUserDetailsService;
import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.constants.Oauth2Constant;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;


/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月8日 下午11:02:30
 * @vesion: 1.0
 */
@Configuration
@RequiredArgsConstructor
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
	
	private final DataSource dataSource;
	private final SecurityUserDetailsService userDetailsService;
	private final AuthenticationManager authenticationManager;
	private final RedisConnectionFactory redisConnectionFactory;

	@Override
	@SneakyThrows
	public void configure(ClientDetailsServiceConfigurer clients) {
		clients.jdbc(dataSource).passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder());
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) {
		oauthServer
			.allowFormAuthenticationForClients()
			.checkTokenAccess("permitAll()");
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
			.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
			.tokenStore(tokenStore())
			.userDetailsService(userDetailsService)
			.authenticationManager(authenticationManager)
			.reuseRefreshTokens(false)
			.pathMapping("/oauth/confirm_access", "/token/confirm_access")
			.exceptionTranslator(new Oauth2ExceptionTranslator());
	}
	@Bean
	public TokenStore tokenStore() {
		RedisTokenStore tokenStore = new RedisTokenStore(redisConnectionFactory);
		tokenStore.setPrefix(Oauth2Constant.PROJECT_OAUTH_ACCESS);
		return tokenStore;
	}

//	@Bean
//	public TokenEnhancer tokenEnhancer() {
//		return (accessToken, authentication) -> {
//			final Map<String, Object> additionalInfo = new HashMap<>(4);
//			LoginUser pigUser = (LoginUser) authentication.getUserAuthentication().getPrincipal();
//			additionalInfo.put(Oauth2Constant.DETAILS_LICENSE, Oauth2Constant.PROJECT_LICENSE);
//			additionalInfo.put(Oauth2Constant.DETAILS_USER_ID, pigUser.getId());
//			additionalInfo.put(Oauth2Constant.DETAILS_USERNAME, pigUser.getUsername());
//			additionalInfo.put(Oauth2Constant.DETAILS_ORGAN_ID, pigUser.getOrganId());
//			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
//			return accessToken;
//		};
//	}
}
