package com.litchi.cloud.auth.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:51:06
 * @vesion: 1.0
 */
@JsonSerialize(using = ICOauthExceptionSerializer.class)
public class ForbiddenException extends ICOauthException {

	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = 281248275022523877L;

	public ForbiddenException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "access_denied";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.FORBIDDEN.value();
	}

}
