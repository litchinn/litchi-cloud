package com.litchi.cloud.auth.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:55:04
 * @vesion: 1.0
 */
@JsonSerialize(using = ICOauthExceptionSerializer.class)
public class ServerErrorException extends ICOauthException {

	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = -3223958367135097828L;

	public ServerErrorException(String msg, Throwable t) {
		super(msg);
	}

	@Override
	public String getOAuth2ErrorCode() {
		return "server_error";
	}

	@Override
	public int getHttpErrorCode() {
		return HttpStatus.INTERNAL_SERVER_ERROR.value();
	}

}
