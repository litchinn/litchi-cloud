package com.litchi.cloud.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.litchi.cloud.auth.beans.CaptchaFilter;

import lombok.SneakyThrows;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午10:03:26
 * @vesion: 1.0
 */
@Primary
@Order(90)
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CaptchaFilter verifyCodeFilter;

	@Override
	@SneakyThrows
	protected void configure(HttpSecurity http) {
		http.addFilterBefore(verifyCodeFilter, UsernamePasswordAuthenticationFilter.class);
		http
			.formLogin()
			//.loginPage("/token/login")
			//.loginProcessingUrl("/token/form")
			.and()
			.authorizeRequests()
			.antMatchers(
				"/token/**",
				"/actuator/**",
				"/mobile/**",
				"/captchaImage").permitAll()
			.anyRequest().authenticated()
			.and().httpBasic()
			.and().csrf().disable();
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/css/**");
	}

	@Bean
	@Override
	@SneakyThrows
	public AuthenticationManager authenticationManagerBean() {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

}
