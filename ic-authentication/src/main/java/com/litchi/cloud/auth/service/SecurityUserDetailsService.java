package com.litchi.cloud.auth.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.beans.UserDetailBean;
import com.litchi.cloud.common.constants.Oauth2Constant;
import com.litchi.cloud.common.constants.UserConstants;
import com.litchi.cloud.common.result.Result;
import com.litchi.cloud.common.service.IUserService;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.common.vo.UserInfo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午10:09:06
 * @vesion: 1.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityUserDetailsService implements UserDetailsService {

	@Reference
	IUserService userService;

	/**
	 * 用户类型web用户
	 */
	private static final int USER_TYPE_WEB = 1;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (StringUtils.isEmpty(username)) {
			return new UserDetailBean();
		}
		LoginUser user = getUserByLoginName(username);
		if (user.getId() == null) {
			throw new UsernameNotFoundException(username);
		}
		return user;
	}


	private LoginUser getUserByLoginName(String username) {
		UserInfo info = userService.getUserInfoByUsername(username);
		return getUserDetails(info);
	}
	
	/**
	 * 构建userdetails
	 *
	 * @param result 用户信息
	 * @return
	 */
	private LoginUser getUserDetails(UserInfo info) {
		if (info == null) {
			throw new UsernameNotFoundException("用户不存在");
		}
		Set<String> dbAuthsSet = new HashSet<>();
		if (ArrayUtils.isNotEmpty(info.getRoles())) {
			// 获取角色
			Arrays.stream(info.getRoles()).forEach(role -> dbAuthsSet.add(Oauth2Constant.ROLE + role));
			// 获取资源
			dbAuthsSet.addAll(Arrays.asList(info.getPermissions()));

		}
		Collection<? extends GrantedAuthority> authorities
			= AuthorityUtils.createAuthorityList(dbAuthsSet.toArray(new String[0]));
		User user = info.getUser();

		// 构造security用户
		return new LoginUser(user.getId(), user.getOrganId(), user.getUserName(), user.getPassword(), user.getNickName(), "", user.getAvatar(),
				UserConstants.NORMAL.equals(user.getDelFlag()), true, true, true, authorities);
	}

	public Result<LoginUser> getUserByLoginName(String username, Integer userType) {
		LoginUser user = getUserByLoginName(username);
		if (user.getId() == null) {
			return Result.error("用户名或密码错误");
		}
		return Result.ok(user);
	}

}
