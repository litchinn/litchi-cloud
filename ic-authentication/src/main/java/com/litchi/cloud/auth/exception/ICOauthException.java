package com.litchi.cloud.auth.exception;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午9:38:24
 * @vesion: 1.0
 */
@JsonSerialize(using = ICOauthExceptionSerializer.class)
public class ICOauthException extends OAuth2Exception {
	
	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = 3481470373367502596L;
	@Getter
	private String errorCode;

	public ICOauthException(String msg) {
		super(msg);
	}

	public ICOauthException(String msg, String errorCode) {
		super(msg);
		this.errorCode = errorCode;
	}
}
