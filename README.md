# litchi-cloud

#### 介绍
springcloud alibaba搭建的微服务权限系统，脚手架，用于快速搭建各种需要权限的服务。

#### 软件架构

##### 相关核心依赖

| 依赖                          | 版本          |
| ----------------------------- | ------------- |
| springboot                    | 2.2.6.RELEASE |
| springcloud alibaba           | 2.2.1.RELEASE |
| springcloud                   | Hoxton.SR5    |
| Mybatis-Plus                  | 3.3.0         |
| jeecg                         | 1.0.7         |
| swagger2                      | 2.8.0         |
| xiaoymin-swagger-bootstrap-ui | 1.9.6         |
| spring-cloud-starter-dubbo    | 2.2.1         |

##### 结构拓扑

![拓扑](img/拓扑.png)

##### 模块说明

~~~ 
ic
├── ic-common ----公用组件
├── ic-system ----系统服务
|	  ├── 用户管理
|	  ├── 角色管理
|	  ├── 资源菜单管理
|	  ├── 机构管理
|	  ├── 字典管理
|	  ├── 通知管理
|	  ├── 日志管理（未完成）
|	  └── 参数设置
├── ic-authentication ----认证中心
└── ic-gateway ----网关
~~~




#### 安装教程

1.  克隆项目即可进行快速开发
2.  所需软件
    1.  MySQL 5.7
    2.  Nacos 1.2.1
    3.  Redis
3.  配置文件
    1.  [system](<https://gitee.com/litchinn/litchi-cloud/blob/master/config-doc/ic-system.yml>)
    2.  [authentication](<https://gitee.com/litchinn/litchi-cloud/blob/master/config-doc/ic-authentication.yml>)
    3.  [gateway](<https://gitee.com/litchinn/litchi-cloud/blob/master/config-doc/ic-gateway.yml>)
4.  详细教程见开发手册(准备中)

#### 未来规划

后续会继续加入任务调度，admin，zipkin等服务

#### 使用说明

1.  随意使用
2.  有问题或者意见请联系941895102@qq.com

#### 参考项目

- [RuoYi-Vue](<https://gitee.com/y_project/RuoYi-Vue>)
- [Pig](<https://gitee.com/log4j/pig>)