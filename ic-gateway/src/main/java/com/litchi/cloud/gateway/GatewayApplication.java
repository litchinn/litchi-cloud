package com.litchi.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 下午11:18:11
 * @vesion: 1.0
 */
@SpringCloudApplication
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}
}
