package com.litchi.cloud.gateway.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月10日 下午1:49:30
 * @vesion: 1.0
 */
@RestController
@RequestMapping("error")
@Slf4j
public class FallbackController {

    @RequestMapping("/fallback")
    public ClientHttpResponse fallback() {
    	ClientHttpResponse errorResult = response(HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("Invoke service failed...");
        return errorResult;
    }
    
    private ClientHttpResponse response(final HttpStatus status) {
		return new ClientHttpResponse() {
			public HttpStatus getStatusCode() throws IOException {
				return status;
			}

			public int getRawStatusCode() throws IOException {
				return status.value();
			}

			public String getStatusText() throws IOException {
				return status.getReasonPhrase();
			}

			public void close() {
			}

			public InputStream getBody() throws IOException {
				return new ByteArrayInputStream("{\"code\":\"500\",\"msg\":\"微服务不可用，请稍后再试。\"}".getBytes());
			}

			public HttpHeaders getHeaders() {
				// headers设定
				HttpHeaders headers = new HttpHeaders();
				MediaType mt = new MediaType("application", "json", Charset.forName("UTF-8"));
				headers.setContentType(mt);
				return headers;
			}
		};
	}
}
