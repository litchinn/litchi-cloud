package com.litchi.cloud.common.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.litchi.cloud.common.beans.LoginUser;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.vo.User;
import com.litchi.cloud.common.vo.UserInfo;
import com.litchi.cloud.common.vo.UserVO;

/**
 * @Description: 用户
 * @Author: jeecg-boot
 * @Date:   2020-04-29
 * @Version: V1.0
 */
public interface IUserService extends IService<User> {

	AjaxResult createUser(UserVO user);

	AjaxResult update(UserVO user);

	LoginUser getUserByUsername(String loginName);

	String selectUserRoleGroup(String username);

	UserInfo getUserInfoByUsername(String username);
}
