package com.litchi.cloud.common.exception;

import com.litchi.cloud.common.enums.ErrorCodeEnum;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月9日 下午2:47:35
 * @vesion: 1.0
 */
public class CryptException extends CustomException {

	private static final long serialVersionUID = -4769180317768163336L;

	public CryptException() {
		super();
	}

	public CryptException(Throwable e) {
		super(e);
	}

	public CryptException(ErrorCodeEnum errorType) {
		super(errorType);
	}

	public CryptException(ErrorCodeEnum errorCode, String... errMsg) {
		super(errorCode, errMsg);
	}
	
	/**
	 * 封装异常
	 * 
	 * @param errorCode
	 * @param errMsg
	 * @param isTransfer
	 *            是否转换异常信息，如果为false,则直接使用errMsg信息
	 */
	public CryptException(ErrorCodeEnum errorCode, String errMsg, Boolean isTransfer) {
		super(errorCode, errMsg, isTransfer);
	}

	public CryptException(ErrorCodeEnum errCode, Throwable cause, String... errMsg) {
		super(errCode, cause, errMsg);
	}
}
