package com.litchi.cloud.common.utils.query;

import javax.servlet.http.HttpSession;

import com.google.common.base.Joiner;
import com.litchi.cloud.common.exception.CustomException;
import com.litchi.cloud.common.utils.ConvertUtils;
import com.litchi.cloud.common.utils.WebContextUtil;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月14日 下午4:12:52
 * @vesion: 1.0
 */
public class JwtUtil {

	// 过期时间30分钟
	public static final long EXPIRE_TIME = 30 * 60 * 1000;

//	/**
//	 * 校验token是否正确
//	 *
//	 * @param token  密钥
//	 * @param secret 用户的密码
//	 * @return 是否正确
//	 */
//	public static boolean verify(String token, String username, String secret) {
//		try {
//			// 根据密码生成JWT效验器
//			Algorithm algorithm = Algorithm.HMAC256(secret);
//			JWTVerifier verifier = JWT.require(algorithm).withClaim("username", username).build();
//			// 效验TOKEN
//			DecodedJWT jwt = verifier.verify(token);
//			return true;
//		} catch (Exception exception) {
//			return false;
//		}
//	}

	/**
	 * // * 获得token中的信息无需secret解密也能获得 // * // * @return token中包含的用户名 //
	 */
//	public static String getUsername(String token) {
//		try {
//			DecodedJWT jwt = JWT.decode(token);
//			return jwt.getClaim("username").asString();
//		} catch (JWTDecodeException e) {
//			return null;
//		}
//	}

//	/**
//	 * 生成签名,5min后过期
//	 *
//	 * @param username 用户名
//	 * @param secret   用户的密码
//	 * @return 加密的token
//	 */
//	public static String sign(String username, String secret) {
//		Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
//		Algorithm algorithm = Algorithm.HMAC256(secret);
//		// 附带username信息
//		return JWT.create().withClaim("username", username).withExpiresAt(date).sign(algorithm);
//
//	}

	/**
	 * 根据request中的token获取用户账号
	 * 
	 * @param request
	 * @return
	 * @throws JeecgBootException
	 */
	public static String getUserNameByToken() throws CustomException {
//		User user = SessionUtil.getCurrentUser();
//		if (oConvertUtils.isEmpty(user)) {
//			throw new BusinessException("未获取到用户");
//		}
		return WebContextUtil.getUsername();
	}

	/**
	 * 从session中获取变量
	 * 
	 * @param key
	 * @return
	 */
	public static String getSessionData(String key) {
		// ${myVar}%
		// 得到${} 后面的值
		String moshi = "";
		if (key.indexOf("}") != -1) {
			moshi = key.substring(key.indexOf("}") + 1);
		}
		String returnValue = null;
		if (key.contains("#{")) {
			key = key.substring(2, key.indexOf("}"));
		}
		if (ConvertUtils.isNotEmpty(key)) {
			HttpSession session = SpringContextUtils.getHttpServletRequest().getSession();
			returnValue = (String) session.getAttribute(key);
		}
		// 结果加上${} 后面的值
		if (returnValue != null) {
			returnValue = returnValue + moshi;
		}
		return returnValue;
	}

	/**
	 * 从当前用户中获取变量
	 * 
	 * @param key
	 * @param user
	 * @return
	 */
	public static String getUserSystemData(String key, SysUserCacheInfo user) {
		if (user == null) {
			user = JeecgDataAutorUtils.loadUserInfo();
		}
		// #{sys_user_code}%
		String moshi = "";
		if (key.indexOf("}") != -1) {
			moshi = key.substring(key.indexOf("}") + 1);
		}
		String returnValue = null;
		// 针对特殊标示处理#{sysOrgCode}，判断替换
		if (key.contains("#{")) {
			key = key.substring(2, key.indexOf("}"));
		}
		// 替换为系统登录用户帐号
		if (key.equals(DataBaseConstant.SYS_USER_CODE) || key.equals(DataBaseConstant.SYS_USER_CODE_TABLE)) {
			returnValue = user.getSysUserCode();
		}
		// 替换为系统登录用户真实名字
		if (key.equals(DataBaseConstant.SYS_USER_NAME) || key.equals(DataBaseConstant.SYS_USER_NAME_TABLE)) {
			returnValue = user.getSysUserName();
		}

		// 替换为系统用户登录所使用的机构编码
		if (key.equals(DataBaseConstant.SYS_ORG_CODE) || key.equals(DataBaseConstant.SYS_ORG_CODE_TABLE)) {
			returnValue = user.getSysOrgCode();
		}
		// 替换为系统用户所拥有的所有机构编码
		if (key.equals(DataBaseConstant.SYS_MULTI_ORG_CODE) || key.equals(DataBaseConstant.SYS_MULTI_ORG_CODE)) {
			if (user.isOneDepart()) {
				returnValue = user.getSysMultiOrgCode().get(0);
			} else {
				returnValue = Joiner.on(",").join(user.getSysMultiOrgCode());
			}
		}
		// 替换为当前系统时间(年月日)
		if (key.equals(DataBaseConstant.SYS_DATE) || key.equals(DataBaseConstant.SYS_DATE_TABLE)) {
			returnValue = user.getSysDate();
		}
		// 替换为当前系统时间（年月日时分秒）
		if (key.equals(DataBaseConstant.SYS_TIME) || key.equals(DataBaseConstant.SYS_TIME_TABLE)) {
			returnValue = user.getSysTime();
		}
		// 流程状态默认值（默认未发起）
		if (key.equals(DataBaseConstant.BPM_STATUS_TABLE) || key.equals(DataBaseConstant.BPM_STATUS_TABLE)) {
			returnValue = "1";
		}
		if (returnValue != null) {
			returnValue = returnValue + moshi;
		}
		return returnValue;
	}
}

