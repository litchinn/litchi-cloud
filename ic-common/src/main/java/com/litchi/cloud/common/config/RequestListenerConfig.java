package com.litchi.cloud.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextListener;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2020/9/21 21:08
 */
@Component
public class RequestListenerConfig {

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}
