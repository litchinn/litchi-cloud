package com.litchi.cloud.common.aop;

import com.litchi.cloud.common.enums.ErrorCodeEnum;
import com.litchi.cloud.common.exception.CustomException;
import com.litchi.cloud.common.result.AjaxResult;
import com.litchi.cloud.common.result.PageResult;
import com.litchi.cloud.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * @author Administrator
 * @version 1.0
 * @description: TODO
 * @date 2020/9/20 12:27
 */
@Aspect
@Component
@Order(1)
@Slf4j
public class ResultAspect {

    @Around(value = "execution(* com.litchi.cloud.*.controller.*.*(..))")
    public Object Around(ProceedingJoinPoint pjp) throws InstantiationException, IllegalAccessException {
        Object response = null;
        StopWatch watch = new StopWatch();
        watch.start();
        long cost = 0;
        try {
            response =  pjp.proceed(pjp.getArgs());
        } catch (Throwable e) {
            log.error("出现错误：", e.getMessage(), e);
            Signature signature = pjp.getSignature();
            Class<?> returnType = ((MethodSignature) signature).getReturnType();
            Object object = returnType.newInstance();
            if (object instanceof Result) {
                if (e instanceof CustomException) {
                    CustomException apiException = (CustomException) e;
                    response = Result.error(apiException.getCode(), apiException.getMessage());
                } else {
                    response = Result.error(ErrorCodeEnum.SYSTEM_ERROR.getCode(), ErrorCodeEnum.SYSTEM_ERROR.getMsg());
                }
            } else if(object instanceof AjaxResult){
                if (e instanceof CustomException) {
                    CustomException apiException = (CustomException) e;
                    response = AjaxResult.error(apiException.getCode(), apiException.getMessage());
                } else {
                    response = AjaxResult.error(ErrorCodeEnum.SYSTEM_ERROR.getCode(), ErrorCodeEnum.SYSTEM_ERROR.getMsg());
                }
            } else if(object instanceof PageResult){
                if (e instanceof CustomException) {
                    CustomException apiException = (CustomException) e;
                    response = PageResult.error(apiException.getCode(), apiException.getMessage());
                } else {
                    response = PageResult.error(ErrorCodeEnum.SYSTEM_ERROR.getCode(), ErrorCodeEnum.SYSTEM_ERROR.getMsg());
                }
            }else {
                response = Result.error(ErrorCodeEnum.BUSINESS_FAIL.getCode(), ErrorCodeEnum.BUSINESS_FAIL.getMsg());
            }
        }
        watch.stop();
        cost = watch.getTotalTimeMillis();
        log.info("Request Processing completed, cost :" + cost);
        return response;
    }
}
