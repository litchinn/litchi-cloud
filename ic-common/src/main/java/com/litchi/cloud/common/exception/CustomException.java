package com.litchi.cloud.common.exception;

import com.litchi.cloud.common.enums.ErrorCodeEnum;

/**
 * 自定义异常
 *
 * @author: wjhf@litchi
 * @date: 2020年5月13日 下午5:21:35
 */
public class CustomException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private Integer code;

    private String message;

    public CustomException(String message) {
        this.message = message;
        this.code = ErrorCodeEnum.BUSINESS_FAIL.getCode();
    }

    public CustomException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public CustomException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }

    public CustomException() {
        super();
    }

    public CustomException(Throwable e) {
        super(e);
    }

    public CustomException(ErrorCodeEnum errCode, String... errMsg) {
        super(errCode.getMsg());
        this.code = errCode.getCode();
        setErrMsg(errMsg, true, errCode);
    }

    public CustomException(ErrorCodeEnum errorCode, String errMsg, Boolean isTransfer) {
        super(errMsg);
        this.code = errorCode.getCode();
        setErrMsg(new String[]{errMsg}, isTransfer, errorCode);
    }

    public CustomException(ErrorCodeEnum errCode, Throwable cause, String[] errMsg) {
        super(errCode.getCode() + errCode.getMsg(), cause);
        this.code = errCode.getCode();
        setErrMsg(errMsg, true, errCode);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public void setErrMsg(String[] errMsg, Boolean isTransfer, ErrorCodeEnum errCode) {

        if (null != errMsg && errMsg.length > 0) {
            if (errCode.getMsg().contains("%s") && isTransfer) {
                this.message = String.format(errCode.getMsg(), errMsg);
            } else {
                StringBuffer sf = new StringBuffer();
                for (String msg : errMsg) {
                    sf.append(msg + ";");
                }
                this.message = sf.toString();
            }
        } else {
            this.message = errCode.getMsg();
        }

    }
}
