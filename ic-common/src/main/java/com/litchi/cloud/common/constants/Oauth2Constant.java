package com.litchi.cloud.common.constants;
/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年5月9日 下午3:05:14
 * @vesion: 1.0
 */
public class Oauth2Constant {

	public static final String PROJECT_OAUTH_ACCESS = "ic";
	
	// scope
	public static final String WECHAT = "wechat";
	public static final String server = "server";
	public static final String WEB = "web";
	
	// ROLE
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
	
	 /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

	public static final String DETAILS_LICENSE = "license";

	public static final Object PROJECT_LICENSE = "made by litchi";

	public static final String DETAILS_USER_ID = "user_id";

	public static final String DETAILS_USERNAME = "username";

	public static final String DETAILS_ORGAN_ID = "organ_id";

	public static final String ROLE = "ROLE_";

	public static final String BCRYPT = "{bcrypt}";
}
