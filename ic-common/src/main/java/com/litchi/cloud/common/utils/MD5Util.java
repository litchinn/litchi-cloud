package com.litchi.cloud.common.utils;

import java.math.BigInteger;
import java.security.MessageDigest;

import com.litchi.cloud.common.enums.ErrorCodeEnum;
import com.litchi.cloud.common.exception.CryptException;

/** 
 * MD5加密
 * @author: wjhf@litchi 
 * @date: 2020年5月9日 下午2:45:38
 * @vesion: 1.0
 */
public class MD5Util {
	
	public static String getMD5(String str) throws Exception {
	    try {
	        // 生成一个MD5加密计算摘要
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        // 计算md5函数
	        md.update(str.getBytes());
	        // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
	        // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
	        return new BigInteger(1, md.digest()).toString(16);
	    } catch (Exception e) {
	        throw new CryptException(ErrorCodeEnum.INVALID_ARGUMENTS, "MD5加密出现错误", false);
	    }
	}
}
