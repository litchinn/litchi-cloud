package com.litchi.cloud.common.beans;

import java.io.Serializable;
import java.util.Collection;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.Data;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年4月29日 上午10:16:34
 * @vesion: 1.0
 */
@Getter
@Setter
public class LoginUser extends User {
	
	private static final long serialVersionUID = 2438645057741884298L;
	private Integer id;
	private String name;
	private String userName;
	private String purviews;// 权限值用 ,分割
	private Integer organId;
	private String ancestors;
	private String organName;

	/**
	 * Construct the <code>User</code> with the details required by
	 * {@link DaoAuthenticationProvider}.
	 *
	 * @param id                    用户ID
	 * @param organId               部门ID
	 * @param username              the username presented to the
	 *                              <code>DaoAuthenticationProvider</code>
	 * @param password              the password that should be presented to the
	 *                              <code>DaoAuthenticationProvider</code>
	 * @param enabled               set to <code>true</code> if the user is enabled
	 * @param accountNonExpired     set to <code>true</code> if the account has not expired
	 * @param credentialsNonExpired set to <code>true</code> if the credentials have not
	 *                              expired
	 * @param accountNonLocked      set to <code>true</code> if the account is not locked
	 * @param authorities           the authorities that should be granted to the caller if they
	 *                              presented the correct username and password and the user is enabled. Not null.
	 * @throws IllegalArgumentException if a <code>null</code> value was passed either as
	 *                                  a parameter or as an element in the <code>GrantedAuthority</code> collection
	 */
	public LoginUser(Integer id, Integer organId, String username, String password, String name, String organName, String ancestors, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.organId = organId;
		this.name = name;
		this.organName = organName;
		this.ancestors = ancestors;
	}
    
	public boolean isAdmin() {
        return isAdmin(this.id);
    }

    public static boolean isAdmin(Integer userId) {
        return userId != null && 1L == userId;
    }
}
