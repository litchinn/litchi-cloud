package com.litchi.cloud.common.vo;

import java.io.Serializable;

import lombok.Data;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年6月9日 上午10:18:57
 * @vesion: 1.0
 */
@Data
public class UserInfo implements Serializable {
	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = 9065169280953081240L;
	/**
	 * 用户基本信息
	 */
	private User user;
	/**
	 * 权限标识集合
	 */
	private String[] permissions;

	/**
	 * 角色集合
	 */
	private String[] roles;
}