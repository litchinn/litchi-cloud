package com.litchi.cloud.common.constants;

import lombok.Data;

/** 
 * 快递API常量
 * @author: wjhf@litchi 
 * @date: 2020年4月26日 上午10:51:10
 * @vesion: 1.0
 */
@Data
public class ExpressConstant {

	private String EXPRESS_API_URL = "kuaidid.market.alicloudapi.com";
}
