package com.litchi.cloud.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @Description: 用户表
 * @Author: jeecg-boot
 * @Date:   2020-05-13
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="user对象", description="用户表")
public class User implements Serializable {
    
	/** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = -5605368909880402934L;
	/**用户ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "用户ID")
	private java.lang.Integer id;
	/**部门ID*/
    @ApiModelProperty(value = "部门ID")
	private java.lang.Integer organId;
	/**用户账号*/
    @ApiModelProperty(value = "用户账号")
	private java.lang.String userName;
	/**用户昵称*/
    @ApiModelProperty(value = "用户昵称")
	private java.lang.String nickName;
	/**用户类型（00系统用户）*/
    @ApiModelProperty(value = "用户类型（00系统用户）")
	private java.lang.String userType;
	/**用户邮箱*/
    @ApiModelProperty(value = "用户邮箱")
	private java.lang.String email;
	/**手机号码*/
    @ApiModelProperty(value = "手机号码")
	private java.lang.String phonenumber;
	/**用户性别（0男 1女 2未知）*/
    @ApiModelProperty(value = "用户性别（0男 1女 2未知）")
	private java.lang.String sex;
	/**头像地址*/
    @ApiModelProperty(value = "头像地址")
	private java.lang.String avatar;
	/**密码*/
    @ApiModelProperty(value = "密码")
	private java.lang.String password;
	/**帐号状态（0正常 1停用）*/
    @ApiModelProperty(value = "帐号状态（0正常 1停用）")
	private java.lang.String status;
	/**删除标志（0代表存在 2代表删除）*/
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
	private java.lang.String delFlag;
	/**最后登陆IP*/
    @ApiModelProperty(value = "最后登陆IP")
	private java.lang.String loginIp;
	/**最后登陆时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "最后登陆时间")
	private java.util.Date loginDate;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
	private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
	private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private java.util.Date updateTime;
	/**备注*/
    @ApiModelProperty(value = "备注")
	private java.lang.String remark;
	
    public boolean isAdmin() {
        return isAdmin(this.id);
    }

    public static boolean isAdmin(Integer userId) {
        return userId != null && 1L == userId;
    }
}
