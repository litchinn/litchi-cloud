package com.litchi.cloud.common.exception;

import com.litchi.cloud.common.enums.ErrorCodeEnum;

/** 
 * RocketMq
 * @author: wjhf@litchi 
 * @date: 2020年5月6日 下午4:12:02
 * @vesion: 1.0
 */
public class RocketMQException extends CustomException {
	private static final long serialVersionUID = 1L;

	/**
	 * 无参构造函数
	 */
	public RocketMQException() {
		super();
	}

	public RocketMQException(Throwable e) {
		super(e);
	}

	public RocketMQException(ErrorCodeEnum errorType) {
		super(errorType);
	}

	public RocketMQException(ErrorCodeEnum errorCode, String... errMsg) {
		super(errorCode, errMsg);
	}

	/**
	 * 封装异常
	 * 
	 * @param errorCode
	 * @param errMsg
	 * @param isTransfer
	 *            是否转换异常信息，如果为false,则直接使用errMsg信息
	 */
	public RocketMQException(ErrorCodeEnum errorCode, String errMsg, Boolean isTransfer) {
		super(errorCode, errMsg, isTransfer);
	}

	public RocketMQException(ErrorCodeEnum errCode, Throwable cause, String... errMsg) {
		super(errCode, cause, errMsg);
	}
}

