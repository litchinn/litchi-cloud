package com.litchi.cloud.common.result;

import java.io.Serializable;

import com.litchi.cloud.common.enums.ErrorCodeEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** 
 * 接口返回对象
 * @author: wjhf@litchi 
 * @date: 2020年4月26日 上午11:26:39
 * @vesion: 1.0
 */
@ApiModel(value = "接口返回对象", description = "接口返回对象")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 8992436576262574064L;

    @ApiModelProperty("返回标识码：（0-成功，1或其他-失败）")
    private Integer code;
    @ApiModelProperty("返回提示信息")
    private String msg;
    @ApiModelProperty("返回实体")
    private T result;
    @ApiModelProperty("时间戳")
    private Long timestamp;
    @ApiModelProperty("跳转url")
    private String gotoUrl;
    
    /**
     * 成功标志
     */
    @ApiModelProperty(value = "成功标志")
    private boolean success = true;
    

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getGotoUrl() {
        return gotoUrl;
    }

    public void setGotoUrl(String gotoUrl) {
        this.gotoUrl = gotoUrl;
    }

    /**
     * 系统定义的错误返回结果
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> error() {
        return error(ErrorCodeEnum.BUSINESS_FAIL.getCode(), ErrorCodeEnum.BUSINESS_FAIL.getMsg());
    }

    /**
     * 自定义返回错误结果 默认错误码为1 业务处理失败
     *
     * @param message
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(String message) {
        return error(ErrorCodeEnum.BUSINESS_FAIL.getCode(), message);
    }
    
    public void error500(String message) {
        this.msg = message;
        this.code = ErrorCodeEnum.SYSTEM_FAIL.getCode();
        this.success = false;
    }

    public void success(String message) {
        this.msg = message;
        this.code = ErrorCodeEnum.RESULT_SUCCESS.getCode();
        this.success = true;
    }

    public static <T> Result<T> ok(String message) {
        return error(ErrorCodeEnum.RESULT_SUCCESS.getCode(), message);
    }

    /**
     * @param code 传入对应的错误码 自动返回对应错误消息
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(Integer code) {
        return error(code, ErrorCodeEnum.getMsg(code));
    }

    /**
     * 全部自定义消息 与错误码
     *
     * @param code
     * @param message
     * @param <T>
     * @return
     */
    public static <T> Result<T> error(Integer code, String message) {
        Result<T> msg = new Result<T>();
        msg.msg = message;
        msg.code = code;
        return msg.putTimeStamp();
    }

    /**
     * 使用系统默认错误码 传入错误返回结果
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> Result<T> errorResult(T result) {
        return new Result<T>().result(result).putTimeStamp().code(ErrorCodeEnum.BUSINESS_FAIL.getCode()).msg(ErrorCodeEnum.BUSINESS_FAIL.getMsg());
    }

    /**
     * 请求成功 默认code为0 掺入对应的返回结果
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> Result<T> ok(T result) {
        return new Result<T>().result(result).putTimeStamp().code(ErrorCodeEnum.RESULT_SUCCESS.getCode()).msg(ErrorCodeEnum.RESULT_SUCCESS.getMsg());
    }
    

    /**
     * 请求成功 默认code为0 掺入对应的返回结果
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> Result<T> ok() {
        return new Result<T>().putTimeStamp().code(ErrorCodeEnum.RESULT_SUCCESS.getCode()).msg(ErrorCodeEnum.RESULT_SUCCESS.getMsg());
    }

    private Result<T> putTimeStamp() {
        this.timestamp = System.currentTimeMillis();
        return this;
    }

    public Result() {

    }

    public Result<T> result(T result) {
        this.result = result;
        return this;
    }

    public Result<T> code(Integer code) {
        this.code = code;
        return this;
    }

    public Result<T> msg(String msg) {
        this.msg = msg;
        return this;
    }

    public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
    //____便于接口返回JSON_____by yuzhiqiang
	public void setSuccess(T object){
        this.success=true;
        this.code=0;
        this.result=object;
    }

	@Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", result=" + result +
                ", timestamp=" + timestamp +
                ", gotoUrl='" + gotoUrl + '\'' +
                '}';
    }
}

