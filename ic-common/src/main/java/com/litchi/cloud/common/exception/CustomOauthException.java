package com.litchi.cloud.common.exception;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/** 
 * 自定义Oauth2异常
 * @author: wjhf@litchi 
 * @date: 2020年5月12日 上午9:51:38
 * @vesion: 1.0
 */
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
public class CustomOauthException extends OAuth2Exception {
    /** 
	 * @Fields serialVersionUID : TODO描述 
	 */ 
	
	private static final long serialVersionUID = 196561445626115825L;

	public CustomOauthException(String msg) {
        super(msg);
    }
}