package com.litchi.cloud.common.beans;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/** 
 * 类说明
 * @author: wjhf@litchi 
 * @date: 2020年4月29日 上午10:09:09
 * @vesion: 1.0
 */
public class UserDetailBean implements UserDetails, CredentialsContainer {
	private static final long serialVersionUID = -8068157018301402L;
	private String password, username, organName, ancestors, name;
	private long organId, userId;
	private boolean accountNonExpired = true, accountNonLocked = true, credentialsNonExpired = true, enabled = true;

	private Collection<GrantedAuthority> authors;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authors;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public long getOrganId() {
		return organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setOrganId(long organId) {
		this.organId = organId;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setAuthors(Collection<GrantedAuthority> collection) {
		this.authors = collection;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public void eraseCredentials() {
		password = null;
	}

	public String getAncestors() {
		return ancestors;
	}

	public void setAncestors(String ancestors) {
		this.ancestors = ancestors;
	}

}
