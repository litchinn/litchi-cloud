package com.litchi.cloud.common.utils;
/** 
 * 获取当前上下文请求信息
 * @author: wjhf@litchi 
 * @date: 2020年5月12日 下午2:45:11
 * @vesion: 1.0
 */

import java.util.Map;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

import com.litchi.cloud.common.beans.UserDetailBean;

public final class WebContextUtil {

	/**
	 * 获取当前上下文授权信息
	 * @return
	 */
	public static Authentication getAuthentication() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			return authentication;
		}
		throw new AuthenticationServiceException("authentication not found");
	}

	/**
	 * 获取当前上下文token的信息
	 * @return
	 */
	public static OAuth2AuthenticationDetails getDetails() {
		Authentication authentication = getAuthentication();
		if (authentication == null) {
			return null;
		}
		OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
		return details;
	}

	/**
	 * 获取当前登入用户账号
	 * @return
	 */
	public static String getUsername() {
		if(getAuthentication().getPrincipal() instanceof Map){
			return ((Map<?, ?>) getAuthentication().getPrincipal()).get("name").toString();
		}
		return getAuthentication().getName();
	}

	/**
	 * 获取当前登入用户的访问accessToken
	 * @return
	 */
	public static String getAccessToken() {
		if (getDetails() == null) {
			return null;
		}
		return getDetails().getTokenValue();
	}

	/**
	 * 获取当前登录用户id
	 * @return 返回类型 Long
	 */
	public static Integer getCurrUid() {
		Map<String, Object> map = null;
		map = getCurrPrincipal();
		if (map != null) {
			return (int) map.get("id");
		}
		return null;
	}

	/**
	 * 获取当前登录用户organId
	 * @return 返回类型 Long
	 */
	public static Integer getOrganId() {
		Map<String, Object> map = null;
		map = getCurrPrincipal();
		if (map != null) {
			return (Integer) map.get("organId");
		}
		return null;
	}

	/**
	 * 获取当前登录用户信息
	 * @return 返回类型 LinkedHashMap<String,Object>
	 */
	public static Map<String, Object> getCurrPrincipal() {
		OAuth2Authentication auth2Authentication = null;
		SecurityContext context = SecurityContextHolder.getContext();
		if (context == null)
			return null;
		Authentication authentication = context.getAuthentication();
		if (authentication == null) {
			return null;
		}
		if (authentication instanceof OAuth2Authentication) {
			auth2Authentication = (OAuth2Authentication) authentication;
		}
		Authentication userAuthentication = auth2Authentication.getUserAuthentication();
		if (userAuthentication == null) {
			return null;
		}
		Object details = userAuthentication.getPrincipal();
		if (details != null) {
			return (Map<String, Object>) details;
		}
		return null;
	}

	public static void clear() {
		SecurityContextHolder.clearContext();
	}

	public static boolean isAdmin(Integer userId) {
		return getCurrUid() == 1;
	}

	public static boolean matchesPassword(String rawPassword, String encodedPassword) {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
	}

	public static String encryptPassword(String password) {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	public static String getCurrUsername() {
		Map<String, Object> map = null;
		map = getCurrPrincipal();
		if (map != null) {
			return map.get("username").toString();
		}
		return null;
	}
}

