package com.litchi.cloud.common.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


/** 
 * 用户对象VO
 * @author: tievd(wjhf)
 * @date: 2019年8月10日 下午4:38:00
 * @vesion: 0.0.1
 */
@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel(value="UserVO",description="用户对象VO")
public class UserVO extends User{

	@ApiModelProperty(value = "角色id")
	private List<Integer> roleIds;
}
